﻿using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace SampleLibrary
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "sampleFunc")]
	public class SampleFunction : IFunction
	{
		public int MaximalArity { get; } = 1;

		public int MinimalArity { get; } = 1;

		public bool Memoizable { get; } = false;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			return scope.Resolve("key1");
		}
	}
}