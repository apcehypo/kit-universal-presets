﻿using System;
using System.ComponentModel.Composition;
using UpLang.Modules;

namespace SampleLibrary
{
	[Export(typeof(Module))]
	[ExportMetadata("Name", "sampleLib")]
	public class SampleModule : Module
	{
		public override string Copyright => "Sample Copyright";

		public override Version Version => new Version(1, 2, 3, 4);
	}
}