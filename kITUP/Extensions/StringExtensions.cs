﻿using System;
using System.Globalization;
using System.Text;

namespace UpLang.Extensions
{
	public static class StringExtensions
	{
		public static string Times(this string str, int count)
		{
			if (count > 0)
			{
				var sb = new StringBuilder(str.Length * count);
				for (; count > 0; count--)
				{
					sb.Append(str);
				}

				return sb.ToString();
			}
			return System.String.Empty;
		}
		
		public static bool StartsWith(this string str, char value, StringComparison comparisonType) => str.StartsWith(value.ToString(), comparisonType);
		public static bool StartsWith(this string str, char value, bool ignoreCase, CultureInfo culture) => str.StartsWith(value.ToString(), ignoreCase, culture);
		public static bool StartsWith(this string str, char value) => str.StartsWith(value.ToString());
	}
}