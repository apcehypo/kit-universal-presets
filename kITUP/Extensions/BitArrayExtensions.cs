﻿using System.Collections;
using System.Text;

namespace UpLang.Extensions
{
	public static class BitArrayExtensions
	{
		public static string AsBinaryString(this BitArray array)
		{
			var sb = new StringBuilder(array.Length);
			for (var i = 0; i < array.Length; i++)
			{
				sb.Append(array[i] ? '1' : '0');
			}

			return sb.ToString();
		}
	}
}