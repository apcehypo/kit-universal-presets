﻿namespace UpLang.Execution
{
	public interface ISectionStream
	{
		Section Current { get; }

		bool HasCurrent { get; }

		bool Consume();

		void Reset();
	}
}