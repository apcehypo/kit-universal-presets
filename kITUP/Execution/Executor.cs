﻿using System;
using System.Collections.Generic;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.Execution
{
	/// <summary>
	///     Исполняет операторы, хранящиеся в скрипте.
	///     Обеспечивает хранение переменных с учётом контекста.
	/// </summary>
	public static class Executor
	{
		public static ModuleRepository Repository = new ModuleRepository();

		public static Stack<IScope> ScopeTrace { get; } = new Stack<IScope>();

		public static void Execute(string code, IScope scope = null)
		{
			var lines = code.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
			var preset = new Preset(lines);
			preset.Execute(scope ?? new PresetScope(null)); //TODO: создавать Executor и Category Scope
		}

#if DEBUG
		public static List<string> DebugLog = new List<string>();

		//Требуется только для тестов вычислимых выражений.
		//В реальности функции не могут вызываться вне контекста.
		static Executor()
		{
			ScopeTrace.Push(new Scope(null));
		}

#endif
	}
}