﻿namespace UpLang.Execution
{
	public class FunctionResult
	{
		public string Value { get; set; }

		public static implicit operator string(FunctionResult result)
		{
			return result != null ? result.Value : System.String.Empty;
		}

		public static string ToString(FunctionResult result)
		{
			return result;
		}

		public static implicit operator FunctionResult(string value)
		{
			return new FunctionResult {Value = value};
		}

		public static FunctionResult FromString(string value)
		{
			return value;
		}
	}
}