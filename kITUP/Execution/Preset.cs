﻿using System;
using System.Collections.Generic;
using System.Linq;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing;
using UpLang.Parsing.Sentences;

namespace UpLang.Execution
{
	/// <summary>
	///     Инкапсулирует секции одного файла и конфигурацию, определённую в файле.
	///     Скрипт нейвно оборачивается в блок [begin]...[end],
	///     а безымянная секция, определяющая настройки, становится оператором [let]
	///     и помещается в начало списка секций.
	/// </summary>
	public class Preset : IStatement, IExecutable
	{
		public Preset(string[] lines)
		{
			Stream = new SectionStream();
			//Stream.Add(new Section(new Header("[begin]")));
			ParseLines(Stream, lines);
			//Stream.Add(new Section(new Header("[end]")));
			Root = Parse(Stream);
		}

		public IExecutable Root { get; }

		public SectionStream Stream { get; }

		public ExecutionResult Execute(IScope scope)
		{
			return Root.Execute(scope);
		}

		public IExecutable Parse(ISectionStream stream)
		{
			//return new BeginEndStatement().Parse(stream);
			var preset = new PresetExecutable();
			while (stream.HasCurrent)
			{
				preset.Executables.Add(stream.Current.Parse(stream));
			}

			return preset;
		}

		public static void ParseLines(SectionStream stream, string[] lines)
		{
			var tuples = lines.Select(x => Parser.ParseLine(x));
			//строки безымянной секции попадают в конфигурацию - секцию [let]
			var target = new Section(new Header("[let]"));
			stream.Add(target);
			foreach (var tuple in tuples)
			{
				if (tuple == null)
				{
					continue;
				}

				switch (tuple.Kind)
				{
					case SentenceType.Comment:
						//ignore
						break;

					case SentenceType.Header:
						target = new Section(tuple.Sentence as Header);
						stream.Add(target);
						break;

					case SentenceType.KeyValue:
						target.Add(tuple.Sentence as KeyValue);
						break;
				}
			}
		}

		public static SectionStream ParseCode(string code)
		{
			var stream = new SectionStream();
			ParseLines(stream, code.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
			return stream;
		}
	}

	public class PresetExecutable : IExecutable
	{
		internal PresetExecutable()
		{
			Executables = new List<IExecutable>();
		}

		internal List<IExecutable> Executables { get; }

		public ExecutionResult Execute(IScope scope)
		{
			var presetScope = scope ?? new PresetScope(null); //TODO: cвязывается с CategoryScope извне
			var results = new CompositeExecutionResult();
			foreach (var executable in Executables)
			{
				results.Add(executable.Execute(presetScope) ?? new ExecutionResult());
			}

			return results;
		}
	}
}