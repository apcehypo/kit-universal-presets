﻿using System.Collections.Generic;

namespace UpLang.Execution
{
	public class SectionStream : ISectionStream
	{
		private int _current;
		private readonly List<Section> _sections = new List<Section>();

		public Section Current
		{
			get
			{
				if (_current >= 0 && _current < _sections.Count)
				{
					return _sections[_current];
				}

				return null;
			}
		}

		public bool Consume()
		{
			if (_current < _sections.Count)
			{
				_current++;
				return true;
			}
			return false;
		}

		public void Reset()
		{
			_current = 0;
		}

		public bool HasCurrent => _current < _sections.Count;

		internal void Add(Section section)
		{
			_sections.Add(section);
		}
	}
}