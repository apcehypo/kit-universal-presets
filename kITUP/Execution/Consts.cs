﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpLang.Execution
{
	public static class Consts
	{
		public const char COMMENT_START_CHAR = ';';
		
		public const char HEADER_START_CHAR = '[';
		
		public const char HEADER_END_CHAR = ']';
		
		public const char SPLITTER_CHAR = '|';
		
		public const char ASSIGN_CHAR = '=';
		
		public const char QUOTE_CHAR = '"';
		
		public const char SUBSTITUTION_CHAR = '`';
		
		public const char ESCAPE_CHAR = '\\';
		
		public const char DICTIONARY_START_CHAR = '%';
		
		public const char DICTIONARY_LITERAL_SEPARATOR = ' ';
		
		public const char DICTIONARY_LITERAL_KEY_VALUE_SEPARATOR = '|';
		
		public const char DICTIONARY_KEY_START_CHAR = '{';
		
		public const char DICTIONARY_KEY_END_CHAR = '}';
		
		public const char FIELD_SEPARATOR_CHAR = '.';
		
		public static readonly char[] WhiteSpaceChars = {' ', '\t'};

		public const string DEFAULT_VARIABLE_NAME = "_";

		public const string DEFAULT_KEY = "";

		public const int DEFAULT_FIELD = 0;

		public const int NO_FIELD = -1;
	}
}
