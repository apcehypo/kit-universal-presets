﻿namespace UpLang.Execution.Scopes
{
	public class PresetScope : Scope
	{
		public PresetScope(IScope parent) : base(parent)
		{
		}

		public override void RedefineInRoot(string name, string value)
		{
			Define(name, value);
		}
	}
}