﻿namespace UpLang.Execution.Scopes
{
	public interface IScope : IScopeBase
	{
		void Define(string dictionary, string varkey, int field, string value);

		void RedefineInRoot(string dictionary, string varkey, int field, string value);

		void Undefine(string dictionary, string varkey, int field);

		string Resolve(string dictionary, string varkey, int field);

		bool Has(string dictionary, string varkey, int field);

		bool Owns(string dictionary, string varkey, int field);
	}
}