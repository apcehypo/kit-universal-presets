﻿namespace UpLang.Execution.Scopes
{
	public interface IScopeBase
	{
		/// <summary>
		///     Define variable.
		/// </summary>
		/// <param name="name">Name of variable.</param>
		/// <param name="value">Value of variable. Use <c>null</c> to undefine variable.</param>
		void Define(string name, string value);

		/// <summary>
		///     Undefine variable in this and all ancestry scopes then define it in the root scope.
		/// </summary>
		/// <param name="name">Name of variable.</param>
		/// <param name="value">Value of variable. Use <c>null</c> to undefine variable.</param>
		void RedefineInRoot(string name, string value);

		void Undefine(string name);

		string Resolve(string name);

		bool Has(string name);

		bool Owns(string name);
	}
}