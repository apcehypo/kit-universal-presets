﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using UpLang.Parsing;

namespace UpLang.Execution.Scopes
{
	public class Scope : IScope
	{
#if DEBUG
		public string ScopeName { get; set; }
#endif

		#region Fields and Constructor

		protected IScope Parent;

		protected SortedDictionary<string, string> Variables;
		protected SortedDictionary<string, SortedDictionary<string, string>> Dictionaries;

		protected SortedDictionary<string, string> VariablesFieldDefaults;
		protected SortedDictionary<string, string> DictionariesDefaults;
		protected SortedDictionary<string, SortedDictionary<string, string>> KeysFieldDefaults;

		public Scope(IScope parent)
		{
			Parent = parent;
			Variables = new SortedDictionary<string, string>();
			Dictionaries = new SortedDictionary<string, SortedDictionary<string, string>>();

			VariablesFieldDefaults = new SortedDictionary<string, string>();
			DictionariesDefaults = new SortedDictionary<string, string>();
			KeysFieldDefaults = new SortedDictionary<string, SortedDictionary<string, string>>();
		}

		#endregion Fields and Constructor

		#region IScope

		public virtual bool Has(string name) => ParseName(name, out var dictionary, out var varkey, out var field) && Has(dictionary, varkey, field);

		public virtual bool Has(string dictionary, string varkey, int field) => Owns(dictionary, varkey, field) || HasParent && Parent.Has(dictionary, varkey, field);

		public virtual bool Owns(string name)
		{
			return ParseName(name, out var dictionary, out var varkey, out var field) && Owns(dictionary, varkey, field);
		}

		public virtual bool Owns(string dictionary, string varkey, int field)
		{
			if (dictionary != null && !Dictionaries.ContainsKey(dictionary))
			{
				return false;
			}

			if (varkey == null)
			{
				return Dictionaries.ContainsKey(dictionary);
			}

			var targetScope = SelectTargetScope(dictionary);
			if (field == Consts.NO_FIELD)
			{
				return targetScope.ContainsKey(varkey);
			}

			if (field == 0)
			{
				return true; //default value always exists
			}

			if (!targetScope.ContainsKey(varkey))
			{
				return false;
			}

			return GetField(targetScope[varkey], field) != null;
		}

		public virtual string Resolve(string name)
		{
			if (ParseName(name, out var dictionary, out var varkey, out var field))
			{
				return Resolve(dictionary, varkey, field);
			}

			throw new Exception(); //TODO: специальное исключение
		}

		public virtual string Resolve(string dictionary, string varkey, int field)
		{
			if (Owns(dictionary, varkey, field))
			{
				if (dictionary != null && varkey == null)
				{
					return String.Join(Consts.DICTIONARY_LITERAL_SEPARATOR.ToString(),
						Dictionaries[dictionary].Select(x => x.Key + Consts.DICTIONARY_LITERAL_KEY_VALUE_SEPARATOR + x.Value).ToArray());
				}

				var targetScope = SelectTargetScope(dictionary);
				switch (field)
				{
					case Consts.NO_FIELD:
						return targetScope[varkey];
					case 0:
						return GetDefault(dictionary, varkey, true);
				}

				return GetField(targetScope[varkey], field) ?? GetDefault(dictionary, varkey);
			}
			if (HasParent && Parent.Has(dictionary, varkey, field))
			{
				return Parent.Resolve(dictionary, varkey, field);
			}

			if (field != Consts.NO_FIELD)
			{
				if (varkey != String.Empty)
				{
					return GetDefault(dictionary, varkey);
				}
				else
				{
					if (KeysFieldDefaults.ContainsKey(dictionary) && KeysFieldDefaults[dictionary].ContainsKey(varkey))
					{
						return KeysFieldDefaults[dictionary][varkey];
					}

					return GetField(GetDefault(dictionary, varkey), field) ?? GetDefault(dictionary, varkey, true);
				}
			}

			return GetDefault(dictionary, String.Empty); //absent key
		}

		public virtual void Define(string name, string value)
		{

			if (ParseName(name, out var dictionary, out var varkey, out var field))
			{
				Define(dictionary, varkey, field, value);
			}
			else
			{
				throw new Exception(); //TODO: специальное исключение
			}
		}

		public virtual void Define(string dictionary, string varkey, int field, string value)
		{
			Contract.Assert(!(dictionary == null && varkey == null)); //not simultaneosly
			var targetScope = SelectTargetScope(dictionary);
			if (varkey != null)
			{
				//ключ словаря
				if (varkey == String.Empty)
				{
					if (field != Consts.NO_FIELD)
					{
						if (field == Consts.DEFAULT_FIELD)
						{
							//like "%dictionary{}.0
							if (!KeysFieldDefaults.ContainsKey(dictionary))
							{
								KeysFieldDefaults.Add(dictionary, new SortedDictionary<string, string>());
							}

							KeysFieldDefaults[dictionary][String.Empty] = value;
						}
						else
						{
							if (dictionary != null)
							{
								if (DictionariesDefaults.ContainsKey(dictionary))
								{
									DictionariesDefaults[dictionary] = SetField(DictionariesDefaults[dictionary], field, value);
								}
								else
								{
									DictionariesDefaults[dictionary] = SetField(String.Empty, field, value);
								}
							}
							else
							{
								throw new Exception(); //TODO: специальное исключение
							}
						}
					}
					else
					{
						DictionariesDefaults[dictionary] = value;
					}
				}
				else
				{
					if (field == 0)
					{
						if (dictionary != null)
						{
							if (!KeysFieldDefaults.ContainsKey(dictionary))
							{
								KeysFieldDefaults.Add(dictionary, new SortedDictionary<string, string>());
							}

							KeysFieldDefaults[dictionary][varkey] = value;
						}
						else
						{
							VariablesFieldDefaults[varkey] = value;
						}
					}
					else
					{
						SetVariable(targetScope, varkey, field, value);
					}
				}
			}
			else
			{
				//сам словарь - парсим литерал-словарь
				targetScope.Clear();
				if (ReferenceEquals(value, null))
				{
					Dictionaries.Remove(dictionary);
				}
				else
				{
					FillDictionary(targetScope, value);
				}
			}
		}

		public virtual void Undefine(string name)
		{
			Define(name, null);
		}

		public virtual void Undefine(string dictionary, string varkey, int field)
		{
			Define(dictionary, varkey, field, null);
		}

		public virtual void RedefineInRoot(string name, string value)
		{

			if (ParseName(name, out var dictionary, out var varkey, out var field))
			{
				RedefineInRoot(dictionary, varkey, field, value);
			}
			else
			{
				throw new Exception(); //TODO: специальное исключение
			}
		}

		public virtual void RedefineInRoot(string dictionary, string varkey, int field, string value)
		{
			if (Parent != null)
			{
				Undefine(dictionary, varkey, field);
				Parent.RedefineInRoot(dictionary, varkey, field, value);
			}
			else
			{
				Define(dictionary, varkey, field, value);
			}
		}

		#endregion IScope

		#region Routines

		protected virtual bool HasParent => Parent != null;

		private void SetDefault(string dictionary, string varkey, string value)
		{
			Contract.Assert(varkey != null); //defaults has only variable/key fields and dictionaries (when key == "")
			if (dictionary != null)
			{
				if (varkey == String.Empty)
				{
					DictionariesDefaults[dictionary] = value;
				}
				else
				{
					if (!KeysFieldDefaults.ContainsKey(dictionary))
					{
						KeysFieldDefaults.Add(dictionary, new SortedDictionary<string, string>());
					}

					KeysFieldDefaults[dictionary][varkey] = value;
				}
			}
			else
			{
				VariablesFieldDefaults[varkey] = value;
			}
		}

		private string GetDefault(string dictionary, string varkey, bool ofKey = false)
		{
			if (dictionary != null)
			{
				if (varkey == String.Empty)
				{
					if (ofKey)
					{
						if (KeysFieldDefaults.ContainsKey(dictionary) && KeysFieldDefaults[dictionary].ContainsKey(varkey))
						{
							return KeysFieldDefaults[dictionary][varkey];
						}

						return String.Empty;
					}
					return DictionariesDefaults.ContainsKey(dictionary) ? DictionariesDefaults[dictionary] : String.Empty;
				}
				else
				{
					if (KeysFieldDefaults.ContainsKey(dictionary) && KeysFieldDefaults[dictionary].ContainsKey(varkey))
					{
						return KeysFieldDefaults[dictionary][varkey];
					}

					return String.Empty;
				}
			}
			//обычная переменная
			if (varkey != null)
			{
				return VariablesFieldDefaults.ContainsKey(varkey) ? VariablesFieldDefaults[varkey] : String.Empty;
			}

			return String.Empty;
		}

		private SortedDictionary<string, string> SelectTargetScope(string dictionary)
		{
			SortedDictionary<string, string> targetScope;
			//определение скопа переменых
			if (dictionary == null)
			{
				//обычная переменная
				targetScope = Variables;
			}
			else
			{
				//словарь
				if (!Dictionaries.ContainsKey(dictionary))
				{
					Dictionaries.Add(dictionary, new SortedDictionary<string, string>());
				}

				targetScope = Dictionaries[dictionary];
			}

			return targetScope;
		}

		private static void SetVariable(SortedDictionary<string, string> targetScope, string varkey, int field, string value)
		{
			if (value != null)
			{
				if (field != Consts.NO_FIELD)
				{
					targetScope[varkey] = SetField(targetScope.ContainsKey(varkey) ? targetScope[varkey] : String.Empty, field, value);
				}
				else
				{
					targetScope[varkey] = value;
				}
			}
			else
			{
				//удаление
				if (field != Consts.NO_FIELD)
				{
					targetScope[varkey] = SetField(targetScope.ContainsKey(varkey) ? targetScope[varkey] : String.Empty, field, null);
				}
				else
				{
					targetScope.Remove(varkey);
				}
			}
		}

		//(?>(\\.|[^\\"])*)
		public static string SetField(string source, int field, string value)
		{
			var fields = new List<string>(new EscapedString(source).Split(Consts.SPLITTER_CHAR));
			if (value != null)
			{
				//замена
				while (field > fields.Count)
				{
					fields.Add(String.Empty); //достраиваем недостающие поля
				}

				fields[field - 1] = value;
			}
			else
			{
				//вырезание
				if (field <= fields.Count)
				{
					fields.RemoveAt(field - 1);
				}
			}
			return String.Join(Consts.SPLITTER_CHAR.ToString(), fields);
		}

		public static string GetField(string source, int field)
		{
			Contract.Assert(field >= 0);
			if (field == 0)
			{
				return null;
			}

			var fields = new List<string>(new EscapedString(source).Split(Consts.SPLITTER_CHAR));
			if (field <= fields.Count)
			{
				return fields[field - 1];
			}

			return null;
		}

		private void FillDictionary(SortedDictionary<string, string> targetScope, string literal)
		{
			Contract.Assert(!ReferenceEquals(literal, null));
			var kvs = new EscapedString(literal).Split(Consts.DICTIONARY_LITERAL_SEPARATOR, false);
			foreach (var item in kvs)
			{
				var kv = new EscapedString(item).Split(Consts.DICTIONARY_LITERAL_KEY_VALUE_SEPARATOR);
				if (kv.Length == 2)
				{
					targetScope.Add(kv[0], kv[1]);
				}
			}
		}

		#endregion Routines

		#region Name parsing

		private enum LogicalState
		{
			Start,
			Variable,
			Dictionary,
			Key,
			AfterKey,
			Field
		}

		public static bool ParseName(string rawName, out string dictionary, out string varkey, out int field)
		{
			dictionary = null;
			varkey = null;
			field = Consts.NO_FIELD;

			var name = new EscapedString(rawName);

			var logicalState = LogicalState.Start;
			var sb = new StringBuilder();

			var index = 0;
			for (; index < name.Value.Length; index++)
			{
				switch (logicalState)
				{
					case LogicalState.Start:
						if (!name.Escaped[index] && name.Value[index] == Consts.DICTIONARY_START_CHAR)
						{
							logicalState = LogicalState.Dictionary;
						}
						else
						{
							logicalState = LogicalState.Variable;
							index--;
						}
						break;

					case LogicalState.Variable:
						if (name.Escaped[index])
						{
							sb.Append(name.Value[index]);
						}
						else
						{
							switch (name.Value[index])
							{
								case Consts.DICTIONARY_KEY_START_CHAR:
									return false; //wrong: "variable{"
								case Consts.DICTIONARY_KEY_END_CHAR:
									return false; //wrong: "variable}"
								case Consts.FIELD_SEPARATOR_CHAR:
									if (sb.Length > 0)
									{
										logicalState = LogicalState.Field;
										varkey = sb.ToString();
										sb.Clear();
									}
									else
									{
										return false; //wrong: "."
									}
									break;

								default:
									sb.Append(name.Value[index]);
									break;
							}
						}

						break;

					case LogicalState.Dictionary:
						if (name.Escaped[index])
						{
							sb.Append(name.Value[index]);
						}
						else
						{
							switch (name.Value[index])
							{
								case Consts.DICTIONARY_KEY_START_CHAR:
									if (sb.Length > 0)
									{
										logicalState = LogicalState.Key;
										dictionary = sb.ToString();
										sb.Clear();
									}
									else
									{
										return false; //wrong: "%{"
									}
									break;

								case Consts.DICTIONARY_KEY_END_CHAR:
									return false; //wrong: "%dictionary}"
								case Consts.FIELD_SEPARATOR_CHAR:
									return false; //wrong: "%dictionary."
								default:
									sb.Append(name.Value[index]);
									break;
							}
						}

						break;

					case LogicalState.Key:
						if (name.Escaped[index])
						{
							sb.Append(name.Value[index]);
						}
						else
						{
							switch (name.Value[index])
							{
								case Consts.DICTIONARY_KEY_END_CHAR:
									logicalState = LogicalState.AfterKey;
									varkey = sb.ToString();
									sb.Clear();
									break;

								default:
									sb.Append(name.Value[index]);
									break;
							}
						}

						break;

					case LogicalState.AfterKey:
						if (name.Escaped[index])
						{
							return false; //wrong: "%dictionary{key}\."
						}
						else
						{
							switch (name.Value[index])
							{
								case Consts.FIELD_SEPARATOR_CHAR:
									logicalState = LogicalState.Field;
									break;

								default:
									return false; //wrong: "%dictionary{key}x"
							}
						}

						break;

					case LogicalState.Field:
						sb.Append(name.Value[index]);
						break;

					default:
						throw new NotImplementedException("ENUM LogicalState");
				}
			}

			switch (logicalState)
			{
				case LogicalState.Start:
					return false; //wrong: empty name
				case LogicalState.Variable:
					varkey = sb.ToString();
					break;

				case LogicalState.Dictionary:
					dictionary = sb.ToString();
					break;

				case LogicalState.Key:
					return false; //wrong: "%dictionary{key"
				case LogicalState.AfterKey:
					//varkey already assigned
					break;

				case LogicalState.Field:
					if (!Int32.TryParse(sb.ToString(), out field))
					{
						return false;
					}

					if (field < Consts.DEFAULT_FIELD)
					{
						return false;
					}

					break;

				default:
					throw new NotImplementedException("ENUM LogicalState");
			}

			return true;
		}

		#endregion Name parsing
	}
}