﻿using System;
using System.Runtime.Serialization;

namespace UpLang.Execution.Exceptions
{
	[Serializable]
	public class LoopException : ExecutionException
	{
		public LoopException()
		{
		}

		public LoopException(string loopAlias) : base(loopAlias)
		{
			LoopAlias = loopAlias;
		}

		public LoopException(string loopAlias, Exception inner) : base(loopAlias, inner)
		{
			LoopAlias = loopAlias;
		}

		protected LoopException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}

		public string LoopAlias { get; set; }
	}

	[Serializable]
	public class LoopStopException : LoopException
	{
		public LoopStopException()
		{
		}

		public LoopStopException(string loopAlias) : base(loopAlias)
		{
		}

		public LoopStopException(string loopAlias, Exception inner) : base(loopAlias, inner)
		{
		}

		protected LoopStopException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}

	[Serializable]
	public class LoopNextException : LoopException
	{
		public LoopNextException()
		{
		}

		public LoopNextException(string loopAlias) : base(loopAlias)
		{
		}

		public LoopNextException(string loopAlias, Exception inner) : base(loopAlias, inner)
		{
		}

		protected LoopNextException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}

	[Serializable]
	public class LoopRedoException : LoopException
	{
		public LoopRedoException()
		{
		}

		public LoopRedoException(string loopAlias) : base(loopAlias)
		{
		}

		public LoopRedoException(string loopAlias, Exception inner) : base(loopAlias, inner)
		{
		}

		protected LoopRedoException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}