﻿using System;
using System.Runtime.Serialization;

namespace UpLang.Execution.Exceptions
{
	[Serializable]
	public class ExecutionException : Exception
	{
		public ExecutionException()
		{
		}

		public ExecutionException(string message) : base(message)
		{
		}

		public ExecutionException(string message, Exception inner) : base(message, inner)
		{
		}

		protected ExecutionException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}