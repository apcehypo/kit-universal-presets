﻿using UpLang.Modules;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Statements
{
	internal struct Condition
	{
		public Text Test { get; }
		public bool Negative { get; }

		public Condition(Text test, bool negative)
		{
			Test = test;
			Negative = negative;
		}

		public static implicit operator bool(Condition condition)
		{
			return (bool) condition.Test ^ condition.Negative;
		}
	}

	internal struct Conditional
	{
		public Condition Condition { get; }
		public IExecutable Executable { get; }

		public Conditional(Condition condition, IExecutable executable)
		{
			Condition = condition;
			Executable = executable;
		}
	}
}