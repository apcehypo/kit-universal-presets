﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	///     Блок операторов:
	///     [begin]
	///     [...]
	///     ...
	///     [end]
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "begin")]
	public class BeginEndStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			//TODO: Проверка stream
			stream.Consume(); //Сразу потребляем секцию [begin]
			var executables = new BeginEndExecutable();
			while (stream.HasCurrent)
			{
				if (stream.Current.Header.Title == "end")
				{
					stream.Consume();
					break;
				}
				else
				{
					executables.Executables.Add(stream.Current.Parse(stream));
				}
			}

			return executables;
		}
	}

	[Export(typeof(IExecutable))]
	public class BeginEndExecutable : IExecutable
	{
		internal BeginEndExecutable()
		{
			Executables = new List<IExecutable>();
		}

		internal List<IExecutable> Executables { get; }

		public ExecutionResult Execute(IScope scope)
		{
			var nestedScope = new Scope(scope);
			Executor.ScopeTrace.Push(nestedScope);
			var results = new CompositeExecutionResult();
			foreach (var executable in Executables)
			{
				results.Add(executable.Execute(nestedScope) ?? new ExecutionResult());
			}

			Executor.ScopeTrace.Pop();
			return results;
		}
	}
}