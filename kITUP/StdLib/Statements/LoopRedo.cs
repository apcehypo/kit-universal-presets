﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Exceptions;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "redo")]
	public class LoopRedoStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			if (stream.Current.Header.Param.Count > 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			var executable = new LoopRedoExecutable(stream.Current.Header.Param.Count == 1 ? stream.Current.Header.Param[0] : null);
			stream.Consume();
			return executable;
		}
	}

	[Export(typeof(IExecutable))]
	public class LoopRedoExecutable : IExecutable
	{
		internal string LoopAlias;

		internal LoopRedoExecutable(string loopAlias)
		{
			LoopAlias = loopAlias;
		}

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			throw LoopAlias == null ? new LoopRedoException() : new LoopRedoException(LoopAlias);
		}
	}
}