﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	///     Условный оператор:
	///     [if|condition] //or [ifnot|...
	///     [block]
	///     [orif|condition] //or [orifnot|...  //optional
	///     [block]
	///     [else] //optional
	///     [block]
	///     Варианты с отрицанием:
	///     вместо if - ifnot
	///     вместо orif - orifnot
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "if")]
	public class IfElseStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}
			//HACK: Технически orif и if обрабатываются одинаково
			stream.Current.Header.Title = new RawText("or" + stream.Current.Header.Title);
			var result = new IfElseExecutable();
			while (stream.HasCurrent)
			{
				var condition = ParseCondition(stream);
				if (condition.HasValue)
				{
					result.Conditionals.Add(new Conditional(condition.Value, ParseExecutable(stream))); //Consumed inside
				}
				else
				{
					break; //Starting an another statement
				}
			}
			return result;
		}

		private Condition? ParseCondition(ISectionStream stream)
		{
			if (stream.Current.Header.Title == "else")
			{
				if (stream.Current.Header.Param.Count != 0)
				{
					throw new Exception(); //TODO: Специальное исключение
				}

				stream.Consume();
				return new Condition(Text.True, false); //Unconditional TRUE
			}
			bool negative;
			if (stream.Current.Header.Title == "orif")
			{
				negative = false;
			}
			else if (stream.Current.Header.Title == "orifnot")
			{
				negative = true;
			}
			else
			{
				return null;
			}

			if (stream.Current.Header.Param.Count != 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			Text test = stream.Current.Header.Param[0];
			stream.Consume();
			return new Condition(test, negative);
		}

		private IExecutable ParseExecutable(ISectionStream stream)
		{
			var action = stream.Current?.Parse(stream); //must Consume()
			if (action == null)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			return action;
		}
	}

	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "ifnot")]
	public class IfNotElseStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			//HACK: Технически orifnot и ifnot обрабатываются одинаково
			stream.Current.Header.Title = new RawText("or" + stream.Current.Header.Title);
			return new IfElseStatement().Parse(stream);
		}
	}

	[Export(typeof(IExecutable))]
	public class IfElseExecutable : IExecutable
	{
		internal Collection<Conditional> Conditionals { get; } = new Collection<Conditional>();

		public ExecutionResult Execute(IScope scope)
		{
			foreach (var conditional in Conditionals)
			{
				if (conditional.Condition)
				{
					return conditional.Executable.Execute(scope);
				}
			}

			return null;
		}
	}
}