﻿using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	///     Empty operator:
	///     []
	///     Does nothing.
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "")]
	public class EmptyOperatorStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			//TODO: Проверить stream
			stream.Consume();
			return new EmptyOperatorExecutable();
		}
	}

	[Export(typeof(IExecutable))]
	public class EmptyOperatorExecutable : IExecutable
	{
		public ExecutionResult Execute(IScope scope)
		{
			return null;
		}
	}
}