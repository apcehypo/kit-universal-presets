﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Exceptions;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "stop")]
	public class LoopStopStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			if (stream.Current.Header.Param.Count > 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			var executable = new LoopStopExecutable(stream.Current.Header.Param.Count == 1 ? stream.Current.Header.Param[0] : null);
			stream.Consume();
			return executable;
		}
	}

	[Export(typeof(IExecutable))]
	public class LoopStopExecutable : IExecutable
	{
		internal string LoopAlias;

		internal LoopStopExecutable(string loopAlias)
		{
			LoopAlias = loopAlias;
		}

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			throw LoopAlias == null ? new LoopStopException() : new LoopStopException(LoopAlias);
		}
	}
}