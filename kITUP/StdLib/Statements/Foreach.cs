﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing;
using UpLang.Parsing.Evaluations;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	/// For each operator:
	/// [foreach|list]
	///  [block]
	/// 
	/// Выполняет block для слова в списке list.
	/// Внутри block доступна переменная по умолчанию "_", содержащая текущий элемент.
	/// Параметр list может быть как литеральной строкой слов, так и выражением, возвращающим список слов.
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "foreach")]
	public class ForeachStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(paramName: nameof(stream));
			}

			if (stream.Current.Header.Param.Count != 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			if (stream.Current.KeyValues.Count != 0)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			var currentVariableName = "_" + (stream.Current.Header.HasAlias ? "_" + stream.Current.Header.Alias : "");
			var expression = stream.Current.Header.Param[0];
			stream.Consume();

			return new ForeachExecutable(expression, currentVariableName, stream.Current.Parse(stream));
		}
	}

	[Export(typeof(IExecutable))]
	public class ForeachExecutable : IExecutable
	{
		internal IExecutable Executable;
		internal TextBase Expression;
		internal string CurrentVariableName;

		internal ForeachExecutable(TextBase expression, string currentVariableName, IExecutable executable)
		{
			Executable = executable;
			Expression = expression;
			CurrentVariableName = currentVariableName;
		}

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(paramName: nameof(scope));
			}

			if (string.IsNullOrEmpty(Expression.Source))
			{
				throw new ArgumentNullException(paramName: nameof(Expression));
			}

			var items = Expression.AsList().ToArray();

			var nestedScope = new Scope(scope);
			Executor.ScopeTrace.Push(nestedScope);
			var results = new CompositeExecutionResult();
			foreach (var item in items)
			{
				nestedScope.Define(Consts.DEFAULT_VARIABLE_NAME, item);
				results.Add(Executable.Execute(nestedScope) ?? new ExecutionResult());
			}
			
			return results;
		}
	}

}