﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	///     Блок операторов:
	///     [let]
	///     key1=value
	///     key2=
	///     key3
	///     ...
	///     Присваивает key значение value в текущем контексте.
	///     key1 - будет value
	///     key2 - будет пустая строка
	///     key3 - будет удалён из контекста
	///     Присвоения выполняются последовательно, строка за строкой.
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "unset")]
	public class UnsetStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			var section = stream.Current;
			stream.Consume();
			return new UnsetExecutable(section);
		}
	}

	[Export(typeof(IExecutable))]
	public class UnsetExecutable : IExecutable
	{
		private readonly Section _section;

		internal UnsetExecutable(Section section)
		{
			Contract.Assert(section.Header.Title == "unset");
			_section = section;
		}

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			switch (_section.Header.Param.Count)
			{
				case 0: //Sectional
					foreach (var line in _section.KeyValues)
					{
						scope.RedefineInRoot(line.Key, null);
					}

					break;

				case 1:
					string param = _section.Header.Param[0];
					if (!String.IsNullOrEmpty(param) && param[0] == Consts.DICTIONARY_START_CHAR)
					{
						//Sectional with dictionary
						if (_section.KeyValues.Count > 0)
						{
							foreach (var line in _section.KeyValues)
							{
								var parts = new EscapedString(line.Key.Source).Split(Consts.FIELD_SEPARATOR_CHAR);
								switch (parts.Length)
								{
									case 0: //default value
										scope.RedefineInRoot(param + "{}", null);
										break;

									case 1: //only key name
										scope.RedefineInRoot(param + "{" + parts[0] + "}", null);
										break;

									case 2: //key name and field number
										scope.RedefineInRoot(param + "{" + parts[0] + "}." + parts[1], null);
										break;

									default:
										throw new Exception(); //TODO: специальное исключение
								}
							}
						}
						else
						{
							scope.RedefineInRoot(_section.Header.Param[0], null);
						}
					}
					else
					{
						//InHeader
						scope.RedefineInRoot(_section.Header.Param[0], null);
						if (_section.KeyValues.Count > 0)
						{
							throw new Exception(); //TODO: специальное исключение
						}
					}
					break;

				default:
					throw new Exception(); //TODO: специальное исключение
			}

			return null;
		}
	}
}