﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Exceptions;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	///     Цикл с предусловием:
	///     [while|condition] //or [until|...
	///     [block]
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "while")]
	public class WhileStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			var result = new WhileExecutable();
			result.LoopAlias = stream.Current.Header.HasAlias ? stream.Current.Header.Alias : null;
			while (stream.HasCurrent)
			{
				var condition = ParseCondition(stream);
				if (condition.HasValue)
				{
					result.Action = new Conditional(condition.Value, ParseExecutable(stream)); //Consumed inside
				}
				else
				{
					break; //Starting an another statement
				}
			}
			return result;
		}

		private Condition? ParseCondition(ISectionStream stream)
		{
			bool negative;
			if (stream.Current.Header.Title == "while")
			{
				negative = false;
			}
			else if (stream.Current.Header.Title == "until")
			{
				negative = true;
			}
			else
			{
				return null;
			}

			if (stream.Current.Header.Param.Count > 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			var test = stream.Current.Header.Param.Count == 1 ? stream.Current.Header.Param[0] : Text.False;
			stream.Consume();
			return new Condition(test, negative);
		}

		private IExecutable ParseExecutable(ISectionStream stream)
		{
			var action = stream.Current?.Parse(stream); //must Consume()
			if (action == null)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			return action;
		}
	}

	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "until")]
	public class UntilStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			return new WhileStatement().Parse(stream);
		}
	}

	[Export(typeof(IExecutable))]
	public class WhileExecutable : IExecutable
	{
		internal string LoopAlias;

		internal Conditional Action { get; set; }

		public ExecutionResult Execute(IScope scope)
		{
			while (Action.Condition)
			{
				LOOP:
				try
				{
					Action.Executable.Execute(scope);
				}
				catch (LoopStopException stop)
				{
					if (stop.LoopAlias == null || stop.LoopAlias == LoopAlias)
					{
						break;
					}
				}
				catch (LoopNextException next)
				{
					if (next.LoopAlias == null || next.LoopAlias == LoopAlias)
					{
						continue;
					}
				}
				catch (LoopRedoException redo)
				{
					if (redo.LoopAlias == null || redo.LoopAlias == LoopAlias)
					{
						goto LOOP;
					}
				}
			}
			return null;
		}
	}
}