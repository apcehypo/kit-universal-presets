﻿using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	/// Local variable assigning operator:
	/// [let]
	/// key1=value
	/// key2=
	/// key3
	/// ...
	/// Присваивает key значение value в текущем контексте.
	/// key1 - будет value
	/// key2 - будет пустая строка
	/// key3 - будет удалён из контекста
	/// Присвоения выполняются последовательно, строка за строкой.
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "let")]
	public class LetStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			//TODO: Проверить stream
			var section = stream.Current;
			stream.Consume();
			return new LetExecutable(section);
		}
	}

	[Export(typeof(IExecutable))]
	public class LetExecutable : IExecutable
	{
		private readonly Section _section;

		internal LetExecutable(Section section)
		{
			Contract.Assert(section.Header.Title == "let");
			_section = section;
		}

		public ExecutionResult Execute(IScope scope)
		{
			//TODO: проверить scope
			//TODO: поддержка хэшей
			foreach (var line in _section.KeyValues)
			{
				if (line.HasAssign)
				{
					scope.Define(line.Key, line.Value);
				}
				else
				{
					scope.Define(line.Key, null);
				}
			}

			return null;
		}
	}
}