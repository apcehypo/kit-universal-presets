﻿#if DEBUG

using System;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "debug")]
	public class DebugStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			var executable = new DebugExecutable(stream.Current);
			stream.Consume();
			return executable;
		}
	}

	[Export(typeof(IExecutable))]
	public class DebugExecutable : IExecutable
	{
		internal Section Section;

		internal DebugExecutable(Section section)
		{
			Contract.Assert(section.Header.Title == "debug");
			Section = section;
		}

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			foreach (var line in Section.KeyValues)
			{
				Executor.DebugLog.Add($"{line.Key}: {scope.Resolve(line.Value)}");
			}

			return null;
		}
	}
}

#endif