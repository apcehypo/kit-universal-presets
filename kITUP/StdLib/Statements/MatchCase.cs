﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Statements
{
	/// <summary>
	/// Match case operator:
	/// [match|expression]
	/// [case|expression]
	///  [block]
	/// ...
	/// [other] //optional
	///  [block]
	/// </summary>
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "match")]
	public class MatchCaseStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			var result = new MatchCaseExecutable();
			if (stream.Current.Header.Param.Count != 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			result.Expression = stream.Current.Header.Param[0];
			stream.Consume();
			while (stream.HasCurrent)
			{
				var caseBlock = ParseCase(stream); //Consumed inside
				if (caseBlock.HasValue)
				{
					result.Cases.Add(caseBlock.Value);
				}
				else
				{
					break; //Starting an another statement
				}
			}
			return result;
		}

		private Case? ParseCase(ISectionStream stream)
		{
			if (stream.Current.Header.Title == "other")
			{
				if (stream.Current.Header.Param.Count != 0)
				{
					throw new Exception(); //TODO: Специальное исключение
				}

				stream.Consume();
				return new Case(null, ParseExecutable(stream)); //Unconditional TRUE
			}
			if (stream.Current.Header.Title == "case")
			{
				if (stream.Current.Header.Param.Count != 1)
				{
					throw new Exception(); //TODO: Специальное исключение
				}

				Text test = stream.Current.Header.Param[0];
				stream.Consume();
				return new Case(test, ParseExecutable(stream));
			}
			return null;
		}

		private IExecutable ParseExecutable(ISectionStream stream)
		{
			var action = stream.Current?.Parse(stream); //must Consume()
			if (action == null)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			return action;
		}
	}

	[Export(typeof(IExecutable))]
	public class MatchCaseExecutable : IExecutable
	{
		internal Text Expression;
		internal Collection<Case> Cases { get; } = new Collection<Case>();

		public ExecutionResult Execute(IScope scope)
		{
			string expr = Expression;
			foreach (var caseBlock in Cases)
			{
				if (caseBlock.IsMatch(expr))
				{
					return caseBlock.Executable.Execute(scope);
				}
			}

			return null;
		}
	}

	internal struct Case
	{
		public Text Test { get; }
		public IExecutable Executable { get; }

		public Case(Text value, IExecutable executable)
		{
			Test = value;
			Executable = executable;
		}

		public bool IsMatch(string value)
		{
			if (ReferenceEquals(Test, null))
			{
				return true; //Unconditional TRUE
			}

			return value == Test;
		}
	}
}