﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Exceptions;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Statements
{
	[Export(typeof(IStatement))]
	[ExportMetadata("Name", "repeat")]
	public class RepeatTimesStatement : IStatement
	{
		public IExecutable Parse(ISectionStream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			if (stream.Current.Header.Param.Count != 1)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			if (stream.Current.KeyValues.Count != 0)
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			if (!Int32.TryParse(stream.Current.Header.Param[0], out var times))
			{
				throw new Exception(); //TODO: Специальное исключение
			}

			var indexVariableName = "_INDEX" + (stream.Current.Header.HasAlias ? "_" + stream.Current.Header.Alias : "");
			stream.Consume();
			return new RepeatTimesExecutable(times, stream.Current.Header.HasAlias ? stream.Current.Header.Alias : null, stream.Current.Parse(stream))
			{
				IndexVariableName = indexVariableName
			};
		}
	}

	[Export(typeof(IExecutable))]
	public class RepeatTimesExecutable : IExecutable
	{
		internal IExecutable Executable;
		internal string LoopAlias;
		internal int Times;

		internal RepeatTimesExecutable(int times, string loopAlias, IExecutable executable)
		{
			Times = times;
			Executable = executable;
			LoopAlias = loopAlias;
		}

		internal string IndexVariableName { get; set; }

		public ExecutionResult Execute(IScope scope)
		{
			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			for (var i = 0; i < Times; i++)
			{
				scope.Define(IndexVariableName, i.ToString()); //TODO: Добавлять псевдоним секции
				try
				{
					Executable.Execute(scope);
				}
				catch (LoopStopException stop)
				{
					if (stop.LoopAlias == null || stop.LoopAlias == LoopAlias)
					{
						break;
					}
				}
				catch (LoopNextException next)
				{
					if (next.LoopAlias == null || next.LoopAlias == LoopAlias)
					{
						continue;
					}
				}
				catch (LoopRedoException redo)
				{
					if (redo.LoopAlias == null || redo.LoopAlias == LoopAlias)
					{
						i--;
					}
				}
				finally
				{
					scope.Undefine(IndexVariableName);
				}
			}
			return new ExecutionResult();
		}
	}
}