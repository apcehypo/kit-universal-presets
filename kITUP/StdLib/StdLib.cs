﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using Module = UpLang.Modules.Module;

namespace UpLang.StdLib
{
	[Export(typeof(Module))]
	[ExportMetadata("Name", "std")]
	public class StdLib : Module
	{
		public override string Copyright
		{
			get { return new Lazy<string>(() => ((AssemblyCopyrightAttribute) GetType().Assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false).FirstOrDefault())?.Copyright).Value; }
		}

		public override Version Version
		{
			get { return new Lazy<Version>(() => GetType().Assembly.GetName().Version).Value; }
		}
	}
}