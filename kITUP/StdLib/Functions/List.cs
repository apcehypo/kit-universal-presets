﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "list")]
	public class List : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 0;

		public int MaximalArity { get; } = int.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			return string.Join(" ", args);
		}
	}
}