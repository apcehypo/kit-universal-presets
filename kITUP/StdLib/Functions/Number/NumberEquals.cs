﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Functions.Number
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "==")]
	public class NumberEquals : IFunction
	{
		public bool Memoizable { get; } = true;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length == 0)
			{
				throw new WrongArityException(this, args.Length);
			}

			var result = true;
			if (!Decimal.TryParse(args[0], out var lastValue))
			{
				throw new ArgumentException();
			}

			for (var i = 1; i < args.Length; i++)
			{
				if (!Decimal.TryParse(args[i], out var value))
				{
					throw new ArgumentException();
				}

				if (lastValue != value)
				{
					result = false;
					break;
				}
			}
			return new FunctionResult {Value = Text.FromBoolean(result)};
		}
	}

	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "!=")]
	public class NumberNotEquals : IFunction
	{
		public bool Memoizable { get; } = true;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length == 0)
			{
				throw new WrongArityException(this, args.Length);
			}

			var result = true;
			if (!Decimal.TryParse(args[0], out var lastValue))
			{
				throw new ArgumentException();
			}

			for (var i = 1; i < args.Length; i++)
			{
				if (!Decimal.TryParse(args[i], out var value))
				{
					throw new ArgumentException();
				}

				if (lastValue == value)
				{
					result = false;
					break;
				}
			}
			return new FunctionResult {Value = Text.FromBoolean(result)};
		}
	}
}