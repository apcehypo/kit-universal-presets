﻿using System;
using System.ComponentModel.Composition;
using System.Globalization;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Number
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "random")]
	public class RandomNumber : IFunction
	{
		[ThreadStatic] private static readonly Random Random;

		static RandomNumber()
		{
			Random = new Random(Guid.NewGuid().GetHashCode());
		}

		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 0;

		public int MaximalArity { get; } = 2;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity || args.Length > MaximalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			switch (args.Length)
			{
				case 0:
					return Random.Next().ToString();
				case 1:
					return Random.Next(Int32.Parse(args[0])).ToString();
				case 2:
				default:
					return Random.Next(Int32.Parse(args[0]), Int32.Parse(args[1])).ToString();
			}
		}
	}

	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "randomf")]
	public class RandomDoubleNumber : IFunction
	{
		[ThreadStatic] private static readonly Random Random;

		static RandomDoubleNumber()
		{
			Random = new Random(Guid.NewGuid().GetHashCode());
		}

		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 0;

		public int MaximalArity { get; } = 2;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity || args.Length > MaximalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			var result = Random.NextDouble();
			switch (args.Length)
			{
				case 0:
					return result.ToString(CultureInfo.InvariantCulture);
				case 1:
					return (result * Double.Parse(args[0], CultureInfo.InvariantCulture)).ToString(CultureInfo.InvariantCulture);
				case 2:
				default:
					var inclusiveMin = Double.Parse(args[0], CultureInfo.InvariantCulture);
					var exclusiveMax = Double.Parse(args[1], CultureInfo.InvariantCulture);
					return (result * (exclusiveMax - inclusiveMin) + inclusiveMin).ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}