﻿using System;
using System.ComponentModel.Composition;
using System.Globalization;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Number
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "max")]
	public class NumberMaximal : IFunction
	{
		public bool Memoizable { get; } = true;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length == 0)
			{
				throw new WrongArityException(this, args.Length);
			}

			if (!Decimal.TryParse(args[0], out var result))
			{
				throw new ArgumentException();
			}

			for (var i = 1; i < args.Length; i++)
			{
				if (!Decimal.TryParse(args[i], out var value))
				{
					throw new ArgumentException();
				}

				if (value > result)
				{
					result = value;
				}
			}
			return new FunctionResult {Value = result.ToString(CultureInfo.InvariantCulture)};
		}
	}
}