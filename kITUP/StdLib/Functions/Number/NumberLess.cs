﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;
using UpLang.Parsing.Texts;

namespace UpLang.StdLib.Functions.Number
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "<")]
	public class NumberLess : IFunction
	{
		public bool Memoizable { get; } = true;

		public int MinimalArity { get; } = 2;

		public int MaximalArity { get; } = Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			if (!Decimal.TryParse(args[0], out var first))
			{
				throw new ArgumentException();
			}

			for (var i = 1; i < args.Length; i++)
			{
				if (!Decimal.TryParse(args[i], out var value))
				{
					throw new ArgumentException();
				}

				if (first >= value)
				{
					return new FunctionResult { Value = Text.False };
				}
			}
			return new FunctionResult {Value = Text.True};
		}
	}

	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "<=")]
	public class NumberLessOrEqual : IFunction
	{
		public bool Memoizable { get; } = true;

		public int MinimalArity { get; } = 2;

		public int MaximalArity { get; } = Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			if (!Decimal.TryParse(args[0], out var first))
			{
				throw new ArgumentException();
			}

			for (var i = 1; i < args.Length; i++)
			{
				if (!Decimal.TryParse(args[i], out var value))
				{
					throw new ArgumentException();
				}

				if (first > value)
				{
					return new FunctionResult { Value = Text.False };
				}
			}
			return new FunctionResult {Value = Text.True};
		}
	}
}