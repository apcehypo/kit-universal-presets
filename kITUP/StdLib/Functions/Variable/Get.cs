﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Extensions;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Variable
{
	/// <summary>
	/// Function: get
	/// </summary>
	/// <example>
	/// Common syntax - single argument
	/// <code>1 (get variable)</code><returns>Value of variable</returns> 
	/// <code>2 (get variable.field)</code><returns>Value of field of variable</returns>
	/// <code>3 (get %dicionary)</code><returns>String representation of dictionary</returns> 
	/// <code>4 (get %dicionary{key})</code><returns>Value of key in dictionary</returns>
	/// <code>5 (get %dicionary{key}.field)</code><returns>Value of field of key in dictionary</returns>
	/// <code>6 (get %dicionary{})</code><returns>Default value for keys in dictionary</returns> 
	/// <code>7 (get %dicionary{}.field)</code><returns>Value of field of default value for keys in dictionary</returns>
	/// Second argument means a default value
	/// <code>1d (get variable default)</code><returns>Value of variable</returns> 
	/// <code>2d (get variable.field default)</code><returns>Value of field of variable</returns>
	/// <code>3d (get %dicionary default)</code><returns>String representation of dictionary</returns> 
	/// <code>4d (get %dicionary{key} default)</code><returns>Value of key in dictionary</returns>
	/// <code>5d (get %dicionary{key}.field default)</code><returns>Value of field of key in dictionary</returns>
	/// <code>6d (get %dicionary{} default)</code><returns>Default value for keys in dictionary</returns> 
	/// <code>7d (get %dicionary{}.field default)</code><returns>Value of field of default value for keys in dictionary</returns>
	/// </example>
	/// <remarks>It's possible to emulate evaluability in single argument get syntax by concat: (get (concat % (get dicionary) { (get key) }. (get field)))</remarks>
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "get")]
	public class GetFunction : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = 2;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity || args.Length > MaximalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			var result = scope.Resolve(args[0]);
			if (args.Length == 2 && System.String.IsNullOrEmpty(result))
			{
				return args[1];
			}

			return result;
		}
	}
}