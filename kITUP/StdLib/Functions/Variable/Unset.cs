﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Variable
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "unset")]
	public class UnsetFunction : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = 1;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length != 1)
			{
				throw new WrongArityException(this, args.Length);
			}

			var name = args[0];
			scope.RedefineInRoot(name, null);
			return System.String.Empty;
		}
	}
}