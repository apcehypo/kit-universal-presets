﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Variable
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "let")]
	public class LetFunction : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = 2;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity || args.Length > MaximalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			var name = args[0];
			var value = args.Length == 2 ? args[1] : null;
			scope.Define(name, value);
			return value ?? System.String.Empty;
		}
	}
}