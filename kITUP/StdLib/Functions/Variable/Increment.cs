﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.Variable
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "+1")]
	public class IncrementFunction : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 1;

		public int MaximalArity { get; } = 1;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length < MinimalArity || args.Length > MaximalArity)
			{
				throw new WrongArityException(this, args.Length);
			}

			if (Int32.TryParse(scope.Resolve(args[0]), out var value))
			{
				return (++value).ToString();
			}

			throw new Exception(); //TODO: Специальное исключение
		}
	}
}