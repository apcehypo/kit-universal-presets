﻿using System;
using System.Linq;

namespace UpLang.StdLib.Functions.String
{
	public static class StringCommonTools
	{
		public static string Concat(params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (args.Length == 0)
			{
				return System.String.Empty;
			}

			if (args.Length == 1)
			{
				return args[0];
			}

			return args.Aggregate(System.String.Empty, (acc, arg) => acc + arg);
		}
	}
}