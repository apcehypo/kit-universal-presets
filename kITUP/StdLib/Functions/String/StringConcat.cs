﻿using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Functions.String
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "concat")]
	public class StringConcat : IFunction
	{
		public bool Memoizable { get; } = true;
		public int MinimalArity { get; } = 0;
		public int MaximalArity { get; } = System.Int32.MaxValue;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			return StringCommonTools.Concat(args);
		}
	}
}