﻿using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;

namespace UpLang.StdLib.Functions.String
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "text")]
	public class StringText : IFunction
	{
		public bool Memoizable { get; } = true;
		public int MinimalArity { get; } = 0;
		public int MaximalArity { get; } = 1;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			return StringCommonTools.Concat(args);
		}
	}
}