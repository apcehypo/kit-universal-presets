﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions.String
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "upcase")]
	public class StringUpCase : IFunction
	{
		public bool Memoizable { get; } = true;
		public int MinimalArity { get; } = 1;
		public int MaximalArity { get; } = 1;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length != 1)
			{
				throw new WrongArityException(this, args.Length);
			}

			return args[0].ToUpperInvariant();
		}
	}
}