﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Modules;
using UpLang.Modules.Exceptions;

namespace UpLang.StdLib.Functions
{
	[Export(typeof(IFunction))]
	[ExportMetadata("Name", "guid")]
	public class GenerateGuid : IFunction
	{
		public bool Memoizable { get; } = false;

		public int MinimalArity { get; } = 0;

		public int MaximalArity { get; } = 0;

		public FunctionResult Execute(IScope scope, params string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			if (scope == null)
			{
				throw new ArgumentNullException(nameof(scope));
			}

			if (args.Length != 0)
			{
				throw new WrongArityException(this, args.Length);
			}

			return Guid.NewGuid().ToString();
		}
	}
}