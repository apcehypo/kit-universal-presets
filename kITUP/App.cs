﻿using System;
using UpLang.Execution;
using UpLang.Execution.Scopes;

// ReSharper disable once CheckNamespace
namespace kITUP
{
	internal class App
	{
		private static void Main()
		{
			foreach (var lib in Executor.Repository.ModulesImports)
			{
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.WriteLine($"\r\nБиблиотека: {lib.Metadata.Name} v{lib.Value.Version} от {lib.Value.Copyright}");
				Console.ResetColor();
				foreach (var func in lib.Value.FunctionsImports)
				{
					Console.ForegroundColor = ConsoleColor.DarkBlue;
					Console.WriteLine($" функция: {func.Metadata.Name} Memoizable:{func.Value.Memoizable} MinArgs:{func.Value.MinimalArity} MaxArgs:{func.Value.MaximalArity}");
					Console.ResetColor();
				}
				foreach (var stat in lib.Value.StatementsImports)
				{
					Console.ForegroundColor = ConsoleColor.DarkMagenta;
					Console.WriteLine($" оператор: {stat.Metadata.Name}");
					Console.ResetColor();
				}
			}
			/////////////////////////
			var code1 = @"
[let]
key1=value1
key2=`key1`2
key3=`key2`3
key4=value4
";
			var scope = new Scope(null);
			Executor.Execute(code1, scope);
			Console.WriteLine($"key1 = '{scope.Resolve("key1")}'");
			Console.WriteLine($"key2 = '{scope.Resolve("key2")}'");
			Console.WriteLine($"key3 = '{scope.Resolve("key3")}'");
			Console.WriteLine($"key4 = '{scope.Resolve("key4")}'");

			var code2 = @"
[let]
key4=`key4`
key5=`key4`
key6=`unknown`
key7=`(sampleLib.sampleFunc)`
";
			Executor.Execute(code2, scope);
			Console.WriteLine($"key4 = '{scope.Resolve("key4")}'");
			Console.WriteLine($"key5 = '{scope.Resolve("key5")}'");
			Console.WriteLine($"key6 = '{scope.Resolve("key6")}'");
			Console.WriteLine($"key7 = '{scope.Resolve("key7")}'");

			/////////////////////////////////
			Console.ReadKey();
		}
	}
}