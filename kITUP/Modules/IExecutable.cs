﻿using UpLang.Execution;
using UpLang.Execution.Scopes;

namespace UpLang.Modules
{
	public interface IExecutable
	{
		ExecutionResult Execute(IScope scope);
	}
}