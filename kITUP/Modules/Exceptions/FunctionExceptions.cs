﻿using System;

namespace UpLang.Modules.Exceptions
{
	[Serializable]
	public class WrongArityException : Exception
	{
		public WrongArityException(IFunction function, int gotCount)
		{
			Function = function;
			GotCount = gotCount;
		}

		public IFunction Function { get; }

		public int GotCount { get; }
	}
}