﻿using System;

namespace UpLang.Modules.Exceptions
{
	[Serializable]
	public class AmbiguousDifinitionException : Exception
	{
		public AmbiguousDifinitionException(string name)
		{
			Name = name;
		}

		public string Name { get; }
	}

	[Serializable]
	public class AmbiguousModuleDifinitionException : Exception
	{
		public AmbiguousModuleDifinitionException(string name, params Module[] modules) : base(name)
		{
			Modules = modules;
		}

		public Module[] Modules { get; }
	}

	[Serializable]
	public class AmbiguousFunctionDifinitionException : AmbiguousDifinitionException
	{
		public AmbiguousFunctionDifinitionException(string name, Module module, params IFunction[] functions) : base(name)
		{
			Module = module;
			Functions = functions;
		}

		public Module Module { get; }
		public IFunction[] Functions { get; }
	}

	[Serializable]
	public class AmbiguousStatementDifinitionException : AmbiguousDifinitionException
	{
		public AmbiguousStatementDifinitionException(string name, Module module, params IStatement[] statements) : base(name)
		{
			Module = module;
			Statements = statements;
		}

		public Module Module { get; }
		public IStatement[] Statements { get; }
	}
}