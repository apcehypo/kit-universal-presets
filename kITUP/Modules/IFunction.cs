﻿using UpLang.Execution;
using UpLang.Execution.Scopes;

namespace UpLang.Modules
{
	public interface IFunctionMetadata
	{
		string Name { get; }
	}

	public interface IFunction
	{
		bool Memoizable { get; }

		int MinimalArity { get; }

		int MaximalArity { get; }

		FunctionResult Execute(IScope scope, params string[] args);
	}
}