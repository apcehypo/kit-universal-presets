﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using UpLang.Modules.Exceptions;

namespace UpLang.Modules
{
	public interface IModuleMetadata
	{
		string Name { get; }
	}

	public abstract class Module
	{
		protected Module()
		{
			var catalog = new AggregateCatalog();
			catalog.Catalogs.Add(new AssemblyCatalog(GetType().Assembly));
			var cc = new CompositionContainer(catalog);
			FunctionsImports = cc.GetExports<IFunction, IFunctionMetadata>();
			StatementsImports = cc.GetExports<IStatement, IStatementMetadata>();

			if (FunctionsImports != null)
			{
				foreach (var item in FunctionsImports)
				{
					if (!Functions.ContainsKey(item.Metadata.Name))
					{
						Functions.Add(item.Metadata.Name, new Lazy<IFunction>(() => item.Value));
					}
					else
					{
						throw new AmbiguousFunctionDifinitionException(item.Metadata.Name, this, Functions[item.Metadata.Name].Value, item.Value);
					}
				}
			}

			if (StatementsImports != null)
			{
				foreach (var item in StatementsImports)
				{
					if (!Statements.ContainsKey(item.Metadata.Name))
					{
						Statements.Add(item.Metadata.Name, new Lazy<IStatement>(() => item.Value));
					}
					else
					{
						throw new AmbiguousStatementDifinitionException(item.Metadata.Name, this, Statements[item.Metadata.Name].Value, item.Value);
					}
				}
			}
		}

		public IEnumerable<Lazy<IFunction, IFunctionMetadata>> FunctionsImports { get; set; }

		public Dictionary<string, Lazy<IFunction>> Functions { get; } = new Dictionary<string, Lazy<IFunction>>();

		public IEnumerable<Lazy<IStatement, IStatementMetadata>> StatementsImports { get; set; }

		public Dictionary<string, Lazy<IStatement>> Statements { get; } = new Dictionary<string, Lazy<IStatement>>();

		public abstract string Copyright { get; }

		public abstract Version Version { get; }
	}
}