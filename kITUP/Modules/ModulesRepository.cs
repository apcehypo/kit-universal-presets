﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using UpLang.Modules.Exceptions;

namespace UpLang.Modules
{
	/// <summary>
	///     Инкапсулирует работу с плагинами - модули с операторами и функциями.
	///     Стандартная библиотека обрабатывается так же как и другие модули.
	/// </summary>
	public class ModuleRepository
	{
		public ModuleRepository()
		{
			var self = GetType().Assembly;

			var catalog = new AggregateCatalog();
			//импортируем из текущей сборки - StdLib
			catalog.Catalogs.Add(new AssemblyCatalog(self));
			//импортируем dll-ки из папки рядом с .exe
			catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(self.Location), "*.dll"));

			var cc = new CompositionContainer(catalog);
			try
			{
				cc.ComposeParts(this);
			}
			catch (CompositionException /*compositionException*/)
			{
				//TODO: отреагировать
				//Console.WriteLine(compositionException.ToString());
			}

			if (ModulesImports != null)
			{
				foreach (var item in ModulesImports)
				{
					if (!Modules.ContainsKey(item.Metadata.Name))
					{
						Modules.Add(item.Metadata.Name, item.Value);
					}
					else
					{
						throw new AmbiguousModuleDifinitionException(item.Metadata.Name, Modules[item.Metadata.Name], item.Value);
					}
				}
			}
		}

		[ImportMany]
		internal IEnumerable<Lazy<Module, IModuleMetadata>> ModulesImports { get; set; }

		[ImportMany]
		internal IEnumerable<Lazy<IExecutable>> Executables { get; set; }

		public Dictionary<string, Module> Modules { get; } = new Dictionary<string, Module>();

		private string DefaultModule { get; } = "std";

		/// <summary>
		///     Разрешает имя функции, находя её в модуле.
		/// </summary>
		/// <param name="name"></param>
		/// <returns>Объект-функцию.</returns>
		public IFunction GetFunction(string name)
		{
			//TODO: TEST
			if (String.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException(nameof(name));
			}

			string moduleName;
			string functionName;
			if (name.Contains("."))
			{
				var dotIndex = name.IndexOf('.');
				moduleName = name.Substring(0, dotIndex);
				functionName = name.Substring(dotIndex + 1);
			}
			else
			{
				moduleName = DefaultModule;
				functionName = name;
			}
			if (Modules.ContainsKey(moduleName))
			{
				var module = Modules[moduleName];
				if (module.Functions.ContainsKey(functionName))
				{
					return module.Functions[functionName].Value;
				}

				throw new Exception(); //TODO: специальное исключение
			}
			throw new Exception(); //TODO: специальное исключение
		}

		//HACK: copy-paste GetFunction
		public IStatement GetStatement(string name)
		{
			//TODO: TEST!!!
			string moduleName;
			string statementName;
			if (name.Contains("."))
			{
				var dotIndex = name.IndexOf('.');
				moduleName = name.Substring(0, dotIndex);
				statementName = name.Substring(dotIndex + 1);
			}
			else
			{
				moduleName = DefaultModule;
				statementName = name;
			}
			if (Modules.ContainsKey(moduleName))
			{
				var module = Modules[moduleName];
				if (module.Statements.ContainsKey(statementName))
				{
					return module.Statements[statementName].Value;
				}

				throw new Exception(); //TODO: специальное исключение
			}
			throw new Exception(); //TODO: специальное исключение
		}
	}
}