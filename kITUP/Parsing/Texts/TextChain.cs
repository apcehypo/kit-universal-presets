using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UpLang.Execution;

namespace UpLang.Parsing.Texts
{
	/// <summary>
	/// </summary>
	public sealed class TextChain : TextBase
	{
		public TextChain(params TextBase[] parts)
		{
			if (parts[0].Source == string.Empty)
			{
				parts = parts.Skip(1).ToArray() ;
			}

			Parts = new Collection<TextBase>(parts);
		}

		public Collection<TextBase> Parts { get; }
		
		public override IEnumerable<TextBase> AsList()
		{
			var secondLevel = Parts.SelectMany(p =>
				p is TextChain
					? ((TextChain) p).Parts
					: p.ToString().Split(Consts.WhiteSpaceChars, StringSplitOptions.RemoveEmptyEntries)
						.Select(s => new RawText(s) as TextBase));
			return secondLevel.Select(t => new RawText(t.ToString().Trim()));
		}

		public override string Source
		{
			get
			{
				var sb = new StringBuilder();
				foreach (var part in Parts)
				{
					sb.Append(part.Source);
				}

				return sb.ToString();
			}
		}

		protected override string Value
		{
			get
			{
				var sb = new StringBuilder();
				foreach (var part in Parts)
				{
					sb.Append(part);
				}

				return sb.ToString();
			}
		}

		public void Append(params TextBase[] parts)
		{
			if (parts == null)
			{
				throw new ArgumentNullException(nameof(parts));
			}

			foreach (var part in parts)
			{
				Parts.Add(part);
			}
		}
	}
}