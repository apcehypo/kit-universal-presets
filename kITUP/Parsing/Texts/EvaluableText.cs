using System;
using System.Collections.Generic;
using System.Linq;
using UpLang.Execution;
using UpLang.Parsing.Evaluations;

namespace UpLang.Parsing.Texts
{
	/// <summary>
	/// </summary>
	public class EvaluableText : TextBase
	{
		private readonly Evaluation _evaluation;
		private string _memoized;

		public EvaluableText(string text)
		{
			SourceText = text;
			if (String.IsNullOrEmpty(text))
			{
				_memoized = "";
			}
			else
			{
				if (text[0] == Evaluation.BRACKET_START_CHAR)
				{
					//������� ���������
					// `(fun arg1 arg2)`
					// `(fun1 arg1 (fun1 arg21 arg22) arg3)`
					var index = 0;
					_evaluation = Evaluation.Parse(text, ref index);
				}
				else
				{
					//�������� ����������
					// `name` -> `(get name)`
					// `name1 name2 name3` -> `(get name1)`
					//�� �������: ����� ����� ������� get � ����������� �����������, ������������ ������ ������������ ���������� �� �������������

					var words = text.Split(Consts.WhiteSpaceChars, StringSplitOptions.RemoveEmptyEntries);
					//TODO: ��������� Evaluation ��������������, ��� ��������
					_evaluation = new Evaluation((RawText) "get", (RawText) words[0]);
				}
			}
		}

		public override string Source => SourceText;

		/// <inheritdoc />
		public override IEnumerable<TextBase> AsList()
		{
			return Value.Split(Consts.WhiteSpaceChars, StringSplitOptions.RemoveEmptyEntries).Select(t => (RawText)t);
		}

		protected override string Value
		{
			get
			{
				if (_memoized == null)
				{
					if (_evaluation.CanMemoize)
					{
						_memoized = _evaluation.Value;
						return _memoized;
					}
					else
					{
						return _evaluation.Value;
					}
				}

				return _memoized;
			}
		}
		
		

		public static explicit operator EvaluableText(string text)
		{
			return new EvaluableText(text);
		}

		public static EvaluableText FromString(string text)
		{
			return new EvaluableText(text);
		}
	}
}