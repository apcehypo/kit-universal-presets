using System;
using System.Collections.Generic;

namespace UpLang.Parsing.Texts
{
	/// <summary>
	/// </summary>
	public sealed class EmptyText : TextBase
	{
		public override string Source => String.Empty;

		protected override string Value => String.Empty;
		
		/// <inheritdoc />
		public override IEnumerable<TextBase> AsList()
		{
			yield break;
		}
	}
}