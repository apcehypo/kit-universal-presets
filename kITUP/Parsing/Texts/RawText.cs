using System;
using System.Collections.Generic;
using System.Linq;
using UpLang.Execution;

namespace UpLang.Parsing.Texts
{
	/// <summary>
	/// </summary>
	public sealed class RawText : TextBase
	{
		public RawText(string text)
		{
			RawSourceText = text;
			SourceText = text;
		}

		public RawText(string source, string text)
		{
			RawSourceText = source;
			SourceText = text;
		}

		private string RawSourceText { get; }

		public override string Source => RawSourceText;

		/// <inheritdoc />
		public override IEnumerable<TextBase> AsList()
		{
			return Value.Split(Consts.WhiteSpaceChars, StringSplitOptions.RemoveEmptyEntries).Select(t => (RawText)t);
		}

		protected override string Value => SourceText;

		public static explicit operator RawText(string text)
		{
			return new RawText(text);
		}

		public static RawText ToRawText(string text)
		{
			return new RawText(text);
		}

		public static explicit operator RawText(char symbol)
		{
			return new RawText(symbol.ToString());
		}

		public static RawText ToRawText(char symbol)
		{
			return new RawText(symbol.ToString());
		}
	}
}