﻿using System.Globalization;
using UpLang.Execution;

namespace UpLang.Parsing.Texts
{
	public abstract class Text
	{
		public static readonly Text True = new RawText("true");
		public static readonly Text False = new RawText("false");
		protected abstract string Value { get; }

		public bool Equals(Text that)
		{
			if (null != that)
			{
				return Value == that.Value;
			}

			return false;
		}

		public static bool operator ==(Text left, Text right)
		{
			if ((object) left == null)
			{
				return (object) right == null;
			}

			return left.Equals(right);
		}

		public static bool operator !=(Text left, Text right)
		{
			if ((object) left == null)
			{
				return (object) right != null;
			}

			return !left.Equals(right);
		}

		public override bool Equals(object obj)
		{
			var that = obj as Text;
			return Equals(that);
		}

		public static implicit operator string(Text text)
		{
			if ((object) text != null)
			{
				return text.Value;
			}

			return null;
		}

		public override string ToString()
		{
			return Value;
		}

		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}

		/// <summary>
		///     The string is considered as logical FALSE if:
		///     1. == null
		///     2. == empty
		///     3. == "0"
		///     4. == "false" in any case
		///     5. == "nil" in any case
		///     Otherwise it is considered as logical TRUE.
		/// </summary>
		/// <param name="text"></param>
		public static explicit operator bool(Text text)
		{
			if (ReferenceEquals(text, null))
			{
				return false;
			}

			var value = ((string) text).Trim(Consts.WhiteSpaceChars).ToLower(CultureInfo.InvariantCulture);
			if (System.String.IsNullOrEmpty(value))
			{
				return false;
			}

			if (value == "0" || value == "false" || value == "nil")
			{
				return false;
			}

			return true;
		}

		public static Text FromBoolean(bool value)
		{
			return value ? True : False;
		}
	}
}