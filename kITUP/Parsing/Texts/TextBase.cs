﻿using System.Collections.Generic;

namespace UpLang.Parsing.Texts
{
	/// <summary>
	/// </summary>
	public abstract class TextBase : Text
	{
		protected string SourceText { get; set; }

		public abstract string Source { get; }

		public static TextChain operator +(TextBase left, TextBase right)
		{
			return new TextChain(left, right);
		}

		public TextChain Add(TextBase that)
		{
			return this + that;
		}

		public abstract IEnumerable<TextBase> AsList();
	}
}