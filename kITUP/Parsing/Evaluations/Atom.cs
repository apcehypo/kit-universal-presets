namespace UpLang.Parsing.Evaluations
{
	public sealed class Atom : Expression
	{
		public Atom(string text)
		{
			SourceText = text;
		}

		public override string Value => SourceText;

		public static explicit operator Atom(string text)
		{
			return new Atom(text);
		}

		public static Atom ToAtom(string text)
		{
			return new Atom(text);
		}
	}
}