﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UpLang.Execution;

namespace UpLang.Parsing.Evaluations
{
	/// <summary>
	/// </summary>
	public class Evaluation : Expression
	{
		public const char BRACKET_START_CHAR = '(';
		public const char BRACKET_END_CHAR = ')';

		private string _memoized;

		private Evaluation()
		{
		}

		public Evaluation(string functor, params string[] arguments)
		{
			Functor = (Atom) functor;
			Arguments = new ReadOnlyCollection<Expression>(arguments.Select(x => (Expression) (Atom) x).ToList());
		}

		public Atom Functor { get; protected set; }

		public ReadOnlyCollection<Expression> Arguments { get; private set; }

		public bool CanMemoize { get; private set; }

		public override string Value => CanMemoize ? (_memoized ?? (_memoized = Evaluate())) : Evaluate();

		public static Evaluation Parse(string line, ref int index)
		{
			//TODO: Проверить line и index

			//сюда должны поступать только выражения в скобках
			if (line[index] != BRACKET_START_CHAR)
			{
				return null;
			}

			var logicalState = LogicalState.Functor;
			//Stack<LogicalState> logicalTrace = new Stack<LogicalState>();
			var physicalState = PhysicalState.Atom;
			var previousPhysicalState = PhysicalState.Atom;
			var sb = new StringBuilder();
			var argument = new List<Expression>();
			var result = new Evaluation();

			//первый символ равен '('
			index++;
			line.SkipWhiteSpaces(ref index);
			for (; index < line.Length; index++)
			{
				switch (logicalState)
				{
					case LogicalState.Functor:
						if (IsAtomEnd(line, index))
						{
							result.Functor = (Atom) sb.ToString();
							sb.Clear();
							logicalState = LogicalState.AnalizingArgument;
							index--; //не прыгаем на следующее выражение
						}
						else
						{
							sb.Append(line[index]);
						}
						break;

					case LogicalState.AnalizingArgument:
						line.SkipWhiteSpaces(ref index);
						switch (line[index])
						{
							case BRACKET_END_CHAR:
								//больше аргументов нет
								result.Arguments = new ReadOnlyCollection<Expression>(argument);
								return result; //закончить разбор
							case BRACKET_START_CHAR:
								//вложенное вычисляемое выражение
								var nested = Parse(line, ref index);
								argument.Add(nested);
								logicalState = LogicalState.AnalizingArgument;
								break;

							default:
								//атом
								logicalState = LogicalState.Argument;
								index--; //пусть разбираются в своём состоянии
								break;
						}
						break;

					case LogicalState.Argument:
						switch (physicalState)
						{
							case PhysicalState.Atom:
								switch (line[index])
								{
									case BRACKET_END_CHAR:
										//последний аргумент
										argument.Add((Atom) sb.ToString());
										result.Arguments = new ReadOnlyCollection<Expression>(argument);
										return result; //закончить разбор
									case Consts.QUOTE_CHAR:
										physicalState = PhysicalState.InQuotes;
										break;

									case Consts.ESCAPE_CHAR:
										previousPhysicalState = physicalState;
										physicalState = PhysicalState.AfterEscape;
										break;

									default:
										if (IsAtomEnd(line, index))
										{
											argument.Add((Atom) sb.ToString());
											sb.Clear();
											logicalState = LogicalState.AnalizingArgument;
										}
										else
										{
											sb.Append(line[index]);
										}
										break;
								}
								break;

							case PhysicalState.AfterEscape:
								physicalState = previousPhysicalState;
								sb.Append(line.GetEscapedChar(index));
								break;

							case PhysicalState.InQuotes:
								switch (line[index])
								{
									case Consts.QUOTE_CHAR:
										physicalState = PhysicalState.Atom;
										break;

									case Consts.ESCAPE_CHAR:
										previousPhysicalState = physicalState;
										physicalState = PhysicalState.AfterEscape;
										break;

									default:
										sb.Append(line[index]);
										break;
								}
								break;

							default:
								throw new NotImplementedException("ENUM PhysicalState");
						}
						break;

					default:
						throw new NotImplementedException("ENUM LogicalState");
				}
			}

			return result;
		}

		private static bool IsAtomEnd(string line, int i) => line[i] == BRACKET_END_CHAR || Consts.WhiteSpaceChars.Contains(line[i]);

		private string Evaluate() => Executor.Repository.GetFunction(Functor).Execute(Executor.ScopeTrace.Peek(), Arguments.Select(x => (string) x).ToArray());

		private enum LogicalState
		{
			Functor,
			AnalizingArgument,
			Argument
		}

		private enum PhysicalState
		{
			Atom,
			AfterEscape,
			InQuotes
		}
	}
}