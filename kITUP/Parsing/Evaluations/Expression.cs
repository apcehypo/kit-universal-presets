namespace UpLang.Parsing.Evaluations
{
	/// <summary>
	/// </summary>
	public abstract class Expression
	{
		protected string SourceText { get; set; }
		public abstract string Value { get; }

		public static implicit operator string(Expression expr)
		{
			if (expr != null)
			{
				return expr.Value;
			}

			return null;
		}

		public override string ToString()
		{
			return Value;
		}
	}
}