namespace UpLang.Parsing.Sentences
{
	/// <summary>
	/// </summary>
	public enum SentenceType
	{
		Comment = 0,
		Header = 1,
		KeyValue = 2
	}
}