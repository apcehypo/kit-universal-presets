using System;
using System.Linq;
using UpLang.Execution;

namespace UpLang.Parsing.Sentences
{
	/// <summary>
	/// </summary>
	public sealed class Comment : Sentence
	{
		public Comment(string line)
		{
			if (line == null)
			{
				throw new ArgumentNullException(); //TODO: ���������
			}

			var i = 0;
			while (i < line.Length && line[i] == Consts.COMMENT_START_CHAR)
			{
				i++;
			}

			while (i < line.Length && Consts.WhiteSpaceChars.Contains(line[i]))
			{
				i++;
			}

			Text = line.Substring(i);
		}

		public string Text { get; }
	}
}