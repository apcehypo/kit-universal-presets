namespace UpLang.Parsing.Sentences
{
	/// <summary>
	/// </summary>
	public class Line
	{
		public SentenceType Kind { get; set; }
		public Sentence Sentence { get; set; }
	}
}