using System;
using System.Text;
using UpLang.Execution;
using UpLang.Parsing.Texts;

namespace UpLang.Parsing.Sentences
{
	/// <summary>
	/// </summary>
	public sealed class KeyValue : Sentence
	{
		public KeyValue(string line)
		{
			if (line == null)
			{
				throw new ArgumentNullException(); //TODO: Сообщение
			}

			Key = new EmptyText();
			Value = new EmptyText();
			var logicalState = LogicalState.Key;
			var physicalState = PhysicalState.Normal;
			var previousPhysicalState = PhysicalState.Normal;
			var sb = new StringBuilder();
			TextBase target = new EmptyText();
			for (var i = 0; i < line.Length; i++)
			{
				switch (physicalState)
				{
					case PhysicalState.Normal:
						switch (line[i])
						{
							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.InQuotes;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString(); //.TrimEnd(Parser.WHITE_SPACE_CHARS);
									sb.Clear();
								}
								break;

							case Consts.SUBSTITUTION_CHAR:
								physicalState = PhysicalState.InSubstitution;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString(); //.TrimEnd(Parser.WHITE_SPACE_CHARS);
									sb.Clear();
								}
								break;

							case Consts.ESCAPE_CHAR:
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							default:
								if (logicalState == LogicalState.Key && line[i] == Consts.ASSIGN_CHAR)
								{
									logicalState = LogicalState.Value;
									if (sb.Length > 0)
									{
										target += (RawText) sb.ToString().TrimEnd(Consts.WhiteSpaceChars);
										sb.Clear();
									}
									Key = target;
									target = new EmptyText();
									HasAssign = true;
									line.SkipNextWhiteSpaces(ref i);
								}
								else
								{
									sb.Append(line[i]);
								}
								break;
						}
						break;

					case PhysicalState.AfterEscape:
						physicalState = previousPhysicalState;
						var escaped = line.GetEscapedChar(i);
						target += new RawText("\\" + escaped, escaped.ToString());
						break;

					case PhysicalState.InQuotes:
						switch (line[i])
						{
							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.Normal;
								var quoted = sb.ToString();
								target += new RawText("\"" + quoted + "\"", quoted);
								sb.Clear();
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = PhysicalState.InQuotes;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									var quotedBefore = sb.ToString();
									target += new RawText("\"" + quotedBefore + "\"", quotedBefore);
									sb.Clear();
								}
								break;

							default:
								sb.Append(line[i]);
								break;
						}
						break;

					case PhysicalState.InSubstitution:
						switch (line[i])
						{
							case Consts.SUBSTITUTION_CHAR:
								physicalState = PhysicalState.Normal;
								target += (EvaluableText) sb.ToString();
								sb.Clear();
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = PhysicalState.InSubstitution;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							default:
								sb.Append(line[i]);
								break;
						}
						break;

					default:
						throw new NotImplementedException("ENUM PhysicalState");
				}
			}
			//строка кончилась
			switch (physicalState)
			{
				case PhysicalState.Normal:
					switch (logicalState)
					{
						case LogicalState.Key:
							if (sb.Length > 0)
							{
								target += (RawText) sb.ToString().TrimEnd(Consts.WhiteSpaceChars);
							}

							Key = target;
							Value = null;
							break;

						case LogicalState.Value:
							if (sb.Length > 0)
							{
								target += (RawText) sb.ToString();
							}

							Value = target;
							break;

						default:
							throw new NotImplementedException("ENUM LogicalState");
					}
					break;

				case PhysicalState.AfterEscape:
					//TODO: ошибка
					break;

				case PhysicalState.InQuotes:
					//TODO: ошибка
					break;

				case PhysicalState.InSubstitution:
					//TODO: ошибка
					break;

				default:
					throw new NotImplementedException("ENUM PhysicalState");
			}
		}

		public TextBase Key { get; }
		public TextBase Value { get; }
		public bool HasAssign { get; }

		private enum LogicalState
		{
			Key,
			Value
		}

		private enum PhysicalState
		{
			Normal,
			AfterEscape,
			InQuotes,
			InSubstitution
		}
	}
}