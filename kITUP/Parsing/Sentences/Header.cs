using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UpLang.Execution;
using UpLang.Parsing.Texts;

namespace UpLang.Parsing.Sentences
{
	/// <summary>
	/// </summary>
	public sealed class Header : Sentence
	{
		public Header(string line)
		{
			if (line == null)
			{
				throw new ArgumentNullException(); //TODO: Сообщение
			}

			var param = new List<TextBase>();
			var logicalState = LogicalState.Header;
			var physicalState = PhysicalState.Normal;
			var previousPhysicalState = PhysicalState.Normal;
			var sb = new StringBuilder();
			TextBase target = new EmptyText();
			//пропускаем 0-ой символ, он равен '['
			for (var i = 1; i < line.Length; i++)
			{
				switch (physicalState)
				{
					case PhysicalState.Normal:
						switch (line[i])
						{
							case Consts.SPLITTER_CHAR:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString().TrimEnd(Consts.WhiteSpaceChars);
									sb.Clear();
								}
								line.SkipNextWhiteSpaces(ref i);
								switch (logicalState)
								{
									case LogicalState.Header:
										logicalState = LogicalState.Param;
										Title = target;
										target = new EmptyText();
										break;

									case LogicalState.Param: //остаётся Param
										param.Add(target);
										target = new EmptyText();
										break;

									case LogicalState.Alias: //остаётся Alias
										sb.Append(line[i]); //пока позволяем иметь символ '|' в псевдонимах
										break;

									default:
										throw new NotImplementedException("ENUM LogicalState");
								}
								break;

							case Consts.HEADER_END_CHAR:
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString().TrimEnd(Consts.WhiteSpaceChars);
									sb.Clear();
								}
								switch (logicalState)
								{
									case LogicalState.Header:
										Title = target;
										break;

									case LogicalState.Param:
										param.Add(target);
										break;

									case LogicalState.Alias:
										//ошибка
										break;

									default:
										throw new NotImplementedException("ENUM LogicalState");
								}
								logicalState = LogicalState.Alias;
								physicalState = PhysicalState.AliasAssign;
								line.SkipNextWhiteSpaces(ref i);
								break;

							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.InQuotes;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							case Consts.SUBSTITUTION_CHAR:
								physicalState = PhysicalState.InSubstitution;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							default:
								sb.Append(line[i]);
								break;
						}
						break;

					case PhysicalState.AfterEscape:
						physicalState = previousPhysicalState;
						target += (RawText) line.GetEscapedChar(i);
						break;

					case PhysicalState.InQuotes:
						switch (line[i])
						{
							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							default:
								sb.Append(line[i]);
								break;
						}
						break;

					case PhysicalState.InSubstitution:
						switch (line[i])
						{
							case Consts.SUBSTITUTION_CHAR:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0)
								{
									target += (EvaluableText) sb.ToString();
									sb.Clear();
								}
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0)
								{
									target += (RawText) sb.ToString();
									sb.Clear();
								}
								break;

							default:
								sb.Append(line[i]);
								break;
						}
						break;

					case PhysicalState.AliasAssign:
						if (line[i] == Consts.ASSIGN_CHAR)
						{
							physicalState = PhysicalState.Normal;
							target = new EmptyText();
							HasAlias = false; //если будет не пусто, станет true
						}
						else
						{
							//ошибка [...]|...
							Alias = null;
							i = line.Length; //закончить разбор
						}
						break;

					default:
						throw new NotImplementedException("ENUM PhysicalState");
				}
			}
			//строка кончилась
			Param = new ReadOnlyCollection<TextBase>(param);
			switch (physicalState)
			{
				case PhysicalState.Normal:
					switch (logicalState)
					{
						case LogicalState.Header:
							//ошибка
							break;

						case LogicalState.Param:
							//ошибка
							break;

						case LogicalState.Alias:
							if (sb.Length > 0)
							{
								target += (RawText) sb.ToString();
							}

							Alias = target;
							HasAlias = true;
							break;

						default:
							throw new NotImplementedException("ENUM LogicalState");
					}
					break;

				case PhysicalState.AliasAssign:

					break;

				case PhysicalState.AfterEscape:
					//ошибка
					break;

				case PhysicalState.InQuotes:
					//ошибка
					break;

				case PhysicalState.InSubstitution:
					//ошибка
					break;

				default:
					throw new NotImplementedException("ENUM PhysicalState");
			}
		}

		public TextBase Title { get; set; }
		public ReadOnlyCollection<TextBase> Param { get; }
		public TextBase Alias { get; }
		public bool HasAlias { get; }

		private enum LogicalState
		{
			Header,
			Param,
			Alias
		}

		private enum PhysicalState
		{
			Normal,
			AfterEscape,
			InQuotes,
			InSubstitution,
			AliasAssign
		}
	}
}