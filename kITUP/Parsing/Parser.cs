﻿using System;
using System.Linq;
using UpLang.Execution;
using UpLang.Parsing.Sentences;

namespace UpLang.Parsing
{
	/// <summary>
	/// </summary>
	public static class Parser
	{
		public static Line ParseLine(string sourceLine)
		{
			if (sourceLine == null)
			{
				throw new ArgumentNullException(nameof(sourceLine));
			}

			var line = sourceLine.Trim(Consts.WhiteSpaceChars);
			if (!String.IsNullOrEmpty(line))
			{
				switch (line[0])
				{
					case Consts.COMMENT_START_CHAR:
						return new Line {Kind = SentenceType.Comment, Sentence = new Comment(line)};

					case Consts.HEADER_START_CHAR:
						return new Line {Kind = SentenceType.Header, Sentence = new Header(line)};

					default:
						return new Line {Kind = SentenceType.KeyValue, Sentence = new KeyValue(line)};
				}
			}

			return null;
		}

		public static void SkipNextWhiteSpaces(this string line, ref int index)
		{
			if (String.IsNullOrEmpty(line))
			{
				throw new ArgumentNullException(nameof(line));
			}

			if (index >= line.Length)
			{
				throw new IndexOutOfRangeException();
			}

			while (index < line.Length - 1 && Consts.WhiteSpaceChars.Contains(line[index + 1]))
			{
				index++;
			}
		}

		public static void SkipWhiteSpaces(this string line, ref int index)
		{
			if (String.IsNullOrEmpty(line))
			{
				throw new ArgumentNullException(nameof(line));
			}

			if (index >= line.Length)
			{
				throw new IndexOutOfRangeException();
			}

			while (index < line.Length && Consts.WhiteSpaceChars.Contains(line[index]))
			{
				index++;
			}
		}

		public static char GetEscapedChar(this string line, int index)
		{
			if (String.IsNullOrEmpty(line))
			{
				throw new ArgumentNullException(nameof(line));
			}

			if (index >= line.Length)
			{
				throw new IndexOutOfRangeException();
			}

			switch (line[index])
			{
				case 't':
					return '\t';

				case 'r':
					return '\r';

				case 'n':
					return '\n';

				default:
					return line[index];
			}
		}
	}
}