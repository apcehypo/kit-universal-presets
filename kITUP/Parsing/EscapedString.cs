﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UpLang.Execution;

namespace UpLang.Parsing
{
	public class EscapedString
	{
		public EscapedString(string value)
		{
			Escaped = new BitArray(value.Length);
			var sb = new StringBuilder(value.Length);
			var physicalState = PhysicalState.Normal;
			var previousPhysicalState = PhysicalState.Normal;
			var resultIndex = 0;
			for (var i = 0; i < value.Length; i++)
			{
				switch (physicalState)
				{
					case PhysicalState.Normal:
						switch (value[i])
						{
							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.InQuotes;
								break;

							case Consts.ESCAPE_CHAR:
								physicalState = PhysicalState.AfterEscape;
								break;

							default:
								sb.Append(value[i]);
								Escaped[resultIndex] = false;
								resultIndex++;
								break;
						}
						break;

					case PhysicalState.AfterEscape:
						physicalState = previousPhysicalState;
						sb.Append(value[i]);
						Escaped[resultIndex] = true;
						resultIndex++;
						break;

					case PhysicalState.InQuotes:
						switch (value[i])
						{
							case Consts.QUOTE_CHAR:
								physicalState = PhysicalState.Normal;
								break;

							case Consts.ESCAPE_CHAR:
								previousPhysicalState = PhysicalState.InQuotes;
								physicalState = PhysicalState.AfterEscape;
								break;

							default:
								sb.Append(value[i]);
								Escaped[resultIndex] = true;
								resultIndex++;
								break;
						}
						break;

					default:
						throw new NotImplementedException("ENUM PhysicalState");
				}
			}

			Escaped.Length = resultIndex;
			Value = sb.ToString();
		}

		public string Value { get; }

		/// <summary>
		///     If true - char was escaped and lost special meainig
		/// </summary>
		public BitArray Escaped { get; }

		public string[] Split(char separator, bool keepEmptyEntries = true)
		{
			var parts = new List<string>();
			var sb = new StringBuilder();
			for (var i = 0; i < Value.Length; i++)
			{
				if (Value[i] == separator && !Escaped[i])
				{
					if (sb.Length > 0 || keepEmptyEntries)
					{
						parts.Add(sb.ToString());
						sb.Clear();
					}
				}
				else
				{
					sb.Append(Value[i]);
				}
			}

			if (sb.Length > 0)
			{
				parts.Add(sb.ToString());
			}

			return parts.ToArray();
		}

		public override string ToString()
		{
			return Value;
		}

		private enum PhysicalState
		{
			Normal,
			AfterEscape,
			InQuotes
		}
	}
}