﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;
using UpLang.Execution.Scopes;

namespace Test
{
	[TestClass]
	public abstract class StatementTestsBase
	{
		[TestInitialize]
		public void Initialize()
		{
			var scope = new PresetScope(null);
			Executor.ScopeTrace.Push(scope);
		}
		
		[TestCleanup]
		public void Cleanup()
		{
			Executor.ScopeTrace.Pop();
		}
	}
}
