﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;
using UpLang.Execution.Scopes;

namespace Test.StdLib.Statements
{
	[TestClass]
	public class EmptyOperatorTests : StatementTestsBase
	{
		[TestMethod]
		public void EmptyOperator()
		{
			var code = @"
[]
[set]
key1=value1
[]
[begin]
		[]
		[set]
		key2=value2
		[]
		[]
		[if|true]
			[]
		[else]
			[]
	[end]
[]
[set]
key3=value3
[]
";
			var scope = Executor.ScopeTrace.Peek();
			Executor.Execute(code, scope);

			Assert.AreEqual("value1", scope.Resolve("key1"));
			Assert.AreEqual("value2", scope.Resolve("key2"));
			Assert.AreEqual("value3", scope.Resolve("key3"));
		}
	}
}