﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Evaluations;

namespace Test.StdLib
{
	[TestClass]
	public class StringTests : FunctionTestsBase
	{
		[TestMethod]
		public void String_Length()
		{
			int index;
			index = 0;
			Assert.AreEqual("1", Evaluation.Parse("(length a)", ref index));
			index = 0;
			Assert.AreEqual("5", Evaluation.Parse("(length abcde)", ref index));
			index = 0;
			Assert.AreEqual("6", Evaluation.Parse("(length \"abc de\")", ref index));
			index = 0;
			Assert.AreEqual("abcde12345", Evaluation.Parse("(let var abcde12345)", ref index));
			index = 0;
			Assert.AreEqual("10", Evaluation.Parse("(length (get var))", ref index));
		}

		[TestMethod]
		public void String_DownCase()
		{
			int index;
			index = 0;
			Assert.AreEqual("aa", Evaluation.Parse("(downcase aA)", ref index));
			index = 0;
			Assert.AreEqual("abcde123xyz", Evaluation.Parse("(downcase abcde123XYZ)", ref index));
			index = 0;
			Assert.AreEqual("абв гдеё привет мир", Evaluation.Parse("(downcase \"абв гдеё ПРИВЕТ МИР\")", ref index));
		}

		[TestMethod]
		public void String_UpCase()
		{
			int index;
			index = 0;
			Assert.AreEqual("AA", Evaluation.Parse("(upcase aA)", ref index));
			index = 0;
			Assert.AreEqual("ABCDE123XYZ", Evaluation.Parse("(upcase abcde123XYZ)", ref index));
			index = 0;
			Assert.AreEqual("АБВ ГДЕЁ ПРИВЕТ МИР", Evaluation.Parse("(upcase \"абв гдеё ПРИВЕТ МИР\")", ref index));
		}
	}
}