﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Evaluations;
using UpLang.Parsing.Texts;

namespace Test.StdLib
{
	[TestClass]
	public class NumberTests : FunctionTestsBase
	{
		[TestMethod]
		public void Functions_NumberAdd()
		{
			int index;
			index = 0;
			Assert.AreEqual("2", Evaluation.Parse("(+ 2)", ref index).Value);
			index = 0;
			Assert.AreEqual("5", Evaluation.Parse("(+ 2 3)", ref index).Value);
			index = 0;
			Assert.AreEqual("95", Evaluation.Parse("(+ 2 3 40 50)", ref index).Value);
		}

		[TestMethod]
		public void Functions_NumberGreater()
		{
			int index;
			index = 0;
			Assert.AreEqual(Text.False, Evaluation.Parse("(> 2 3)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.False, Evaluation.Parse("(>= 2 3)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.True, Evaluation.Parse("(> 4 3)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.True, Evaluation.Parse("(>= 4 3)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.True, Evaluation.Parse("(> 4 3 2 1 -10)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.False, Evaluation.Parse("(> 4 3 2 1 10)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.False, Evaluation.Parse("(> 4 3 2 1 4)", ref index).Value);
			index = 0;
			Assert.AreEqual(Text.True, Evaluation.Parse("(>= 4 3 2 1 4)", ref index).Value);
		}

		[TestMethod]
		public void Functions_Decrement()
		{
			int index;
			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(let var 3)", ref index).Value);
			index = 0;
			Assert.AreEqual("2", Evaluation.Parse("(-1 var)", ref index).Value);
			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(get var)", ref index).Value);
			index = 0;
			Assert.AreEqual("2", Evaluation.Parse("(let var (-1 var))", ref index).Value);
			index = 0;
			Assert.AreEqual("2", Evaluation.Parse("(get var)", ref index).Value);
		}

		[TestMethod]
		public void Functions_Increment()
		{
			int index;
			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(let var 3)", ref index).Value);
			index = 0;
			Assert.AreEqual("4", Evaluation.Parse("(+1 var)", ref index).Value);
			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(get var)", ref index).Value);
			index = 0;
			Assert.AreEqual("4", Evaluation.Parse("(let var (+1 var))", ref index).Value);
			index = 0;
			Assert.AreEqual("4", Evaluation.Parse("(get var)", ref index).Value);
		}
	}
}