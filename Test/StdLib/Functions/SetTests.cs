﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Evaluations;

namespace Test.StdLib.Functions
{
	[TestClass]
	public class SetTests : FunctionTestsBase
	{
		[TestMethod]
		public void SetFunction_Variable()
		{
			int index;
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(set var value)", ref index).Value);
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get var)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(set var)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var)", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_VariableField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set var f1|\"f 2\"|f3)", ref index).Value);

			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(set var.2 surprise)", ref index).Value);
			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(get var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|surprise|f3", Evaluation.Parse("(get var)", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_Dictionary()
		{
			int index;
			index = 0;
			Assert.AreEqual("k1|v1 k2|v2 k3|v3", Evaluation.Parse("(set %dictionary \"k1|v1 k2|v2 k3|v3\")", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);
			index = 0;
			Assert.AreEqual("v1", Evaluation.Parse("(get %dictionary{k1})", ref index).Value);
			index = 0;
			Assert.AreEqual("v2", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);
			index = 0;
			Assert.AreEqual("v3", Evaluation.Parse("(get %dictionary{k3})", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_DictionaryKey()
		{
			int index;
			index = 0;
			Assert.AreEqual("k1|v1 k2|v2 k3|v3", Evaluation.Parse("(set %dictionary \"k1|v1 k2|v2 k3|v3\")", ref index).Value);
			index = 0;
			Assert.AreEqual("v2", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);

			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(set %dictionary{k2} surprise)", ref index).Value);
			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_DictionaryKeyField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set %dictionary{key} f1|\"f 2\"|f3)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get %dictionary{key}.2)", ref index).Value);

			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(set %dictionary{key}.2 surprise)", ref index).Value);
			index = 0;
			Assert.AreEqual("surprise", Evaluation.Parse("(get %dictionary{key}.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|surprise|f3", Evaluation.Parse("(get %dictionary{key})", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_DictionaryDefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("default1", Evaluation.Parse("(set %dictionary{} default1)", ref index).Value);
			index = 0;
			Assert.AreEqual("default1", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_FieldDefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set var \"f1|f 2|f3\")", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(set var.0 default)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get var.5)", ref index).Value);
		}

		[TestMethod]
		public void SetFunction_DictionaryDefaultValue_FieldDefaultValuee()
		{
			int index;
			index = 0;
			Assert.AreEqual("default1", Evaluation.Parse("(set %dictionary{key}.0 default1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set %dictionary{} \"f1|f 2|f3\")", ref index).Value);
			index = 0;
			Assert.AreEqual("surpise", Evaluation.Parse("(set %dictionary{}.2 surpise)", ref index).Value);
			index = 0;
			Assert.AreEqual("default2", Evaluation.Parse("(set %dictionary{}.0 default2)", ref index).Value);

			index = 0;
			Assert.AreEqual("default1", Evaluation.Parse("(get %dictionary{key}.5)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|surpise|f3", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("default2", Evaluation.Parse("(get %dictionary{}.5)", ref index).Value);
		}
	}
}