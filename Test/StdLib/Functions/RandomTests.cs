﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Evaluations;

namespace Test.StdLib.Functions
{
	// Use snippets "testc" and "testm" to create unit tests quickly.

	[TestClass]
	public class RandomTests : FunctionTestsBase
	{
		[TestMethod]
		public void RandomNumberTest()
		{
			int index;
			int value;
			for (var i = 0; i < 10 /*00000*/; i++)
			{
				index = 0;
				value = Int32.Parse(Evaluation.Parse("(random)", ref index).Value);
				Assert.IsTrue(value >= 0);
				Assert.IsTrue(value < Int32.MaxValue);

				index = 0;
				value = Int32.Parse(Evaluation.Parse("(random 100)", ref index).Value);
				Assert.IsTrue(value >= 0);
				Assert.IsTrue(value < 100);

				index = 0;
				value = Int32.Parse(Evaluation.Parse("(random -100 200)", ref index).Value);
				Assert.IsTrue(value >= -100);
				Assert.IsTrue(value < 200);

				index = 0;
				Assert.AreEqual(0, Int32.Parse(Evaluation.Parse("(random 1)", ref index).Value));

				index = 0;
				Assert.AreEqual(6, Int32.Parse(Evaluation.Parse("(random 6 7)", ref index).Value));
			}
		}

		[TestMethod]
		public void RandomDoubleNumberTest()
		{
			int index;
			string strval;
			double value;

			for (var i = 0; i < 10 /*00000*/; i++)
			{
				index = 0;
				strval = Evaluation.Parse("(randomf)", ref index).Value;
				value = Double.Parse(strval, CultureInfo.InvariantCulture);
				Assert.IsTrue(value >= 0);
				Assert.IsTrue(value < Double.MaxValue);

				index = 0;
				strval = Evaluation.Parse("(randomf 0.75)", ref index).Value;
				value = Double.Parse(strval, CultureInfo.InvariantCulture);
				Assert.IsTrue(value >= 0);
				Assert.IsTrue(value < 0.75);

				index = 0;
				strval = Evaluation.Parse("(randomf -10.5 -9.005)", ref index).Value;
				value = Double.Parse(strval, CultureInfo.InvariantCulture);
				Assert.IsTrue(value >= -10.5);
				Assert.IsTrue(value < -9.005);
			}
		}

		[TestMethod]
		public void GenerateGuidTest()
		{
			int index;
			index = 0;
			Assert.IsTrue(Guid.TryParse(Evaluation.Parse("(guid)", ref index).Value, out var guid));
		}
	}
}