﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Parsing.Evaluations;

namespace Test.StdLib.Functions
{
	[TestClass]
	public class UnsetTests : FunctionTestsBase
	{
		[TestMethod]
		public void UnsetFunction_Variable()
		{
			int index;
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(set var value)", ref index).Value);
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get var)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(unset var)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var)", ref index).Value);
		}

		[TestMethod]
		public void UnsetFunction_VariableField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set var f1|\"f 2\"|f3)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(unset var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get var.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|f3", Evaluation.Parse("(get var)", ref index).Value);
		}

		[TestMethod]
		public void UnsetFunction_Dictionary()
		{
			int index;
			index = 0;
			Assert.AreEqual("k1|v1 k2|v2 k3|v3", Evaluation.Parse("(set %dictionary \"k1|v1 k2|v2 k3|v3\")", ref index).Value);

			index = 0;
			Assert.AreEqual("v1", Evaluation.Parse("(get %dictionary{k1})", ref index).Value);
			index = 0;
			Assert.AreEqual("v2", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);
			index = 0;
			Assert.AreEqual("v3", Evaluation.Parse("(get %dictionary{k3})", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(unset %dictionary)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{k1})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{k3})", ref index).Value);
		}

		[TestMethod]
		public void UnsetFunction_DictionaryKey()
		{
			int index;
			index = 0;
			Assert.AreEqual("k1|v1 k2|v2 k3|v3", Evaluation.Parse("(set %dictionary \"k1|v1 k2|v2 k3|v3\")", ref index).Value);
			index = 0;
			Assert.AreEqual("v2", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(unset %dictionary{k2})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{k2})", ref index).Value);
			index = 0;
			Assert.AreEqual("k1|v1 k3|v3", Evaluation.Parse("(get %dictionary)", ref index).Value);
		}

		[TestMethod]
		public void UnsetFunction_DictionaryKeyField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(set %dictionary{key} f1|\"f 2\"|f3)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get %dictionary{key}.2)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(unset %dictionary{key}.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get %dictionary{key}.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get %dictionary{key}.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1|f3", Evaluation.Parse("(get %dictionary{key})", ref index).Value);
		}
	}
}