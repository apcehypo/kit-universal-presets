﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;
using UpLang.Execution.Scopes;
using UpLang.Parsing.Evaluations;

namespace Test.StdLib.Functions
{
	[TestClass]
	public class GetTests : FunctionTestsBase
	{
		[TestMethod]
		public void GetFunction_Variable()
		{
			int index;
			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(let x 3)", ref index).Value);
			index = 0;
			Assert.AreEqual("super value", Evaluation.Parse("(let \"super var\" \"super value\")", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(let empty \"\")", ref index).Value);

			index = 0;
			Assert.AreEqual("3", Evaluation.Parse("(get x)", ref index).Value);
			index = 0;
			Assert.AreEqual("super value", Evaluation.Parse("(get \"super var\")", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get empty)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_VariableField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(let var f1|\"f 2\"|f3)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var.0)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get var.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get var.3)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var.4)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_Dictionary()
		{
			int index;
			index = 0;
			Assert.AreEqual("value1", Evaluation.Parse("(let %dictionary{key1} value1)", ref index).Value);
			index = 0;
			Assert.AreEqual("value3", Evaluation.Parse("(let %dictionary{key3} value3)", ref index).Value);
			index = 0;
			Assert.AreEqual("value2", Evaluation.Parse("(let %dictionary{key2} value2)", ref index).Value);

			index = 0;
			Assert.AreEqual("key1|value1 key2|value2 key3|value3", Evaluation.Parse("(get %dictionary)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_DictionaryKey()
		{
			int index;
			index = 0;
			Assert.AreEqual("value1", Evaluation.Parse("(let %dictionary{key1} value1)", ref index).Value);
			index = 0;
			Assert.AreEqual("value 2", Evaluation.Parse("(let %dictionary{key2} \"value 2\")", ref index).Value);
			index = 0;
			Assert.AreEqual("value3", Evaluation.Parse("(let %dictionary{\"key 3\"} value3)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("value1", Evaluation.Parse("(get %dictionary{key1})", ref index).Value);
			index = 0;
			Assert.AreEqual("value 2", Evaluation.Parse("(get %dictionary{key2})", ref index).Value);
			index = 0;
			Assert.AreEqual("value3", Evaluation.Parse("(get %dictionary{key\\ 3})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_DictionaryKeyField()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(let %dictionary{key} f1|\"f 2\"|f3)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{key}.0)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get %dictionary{key}.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get %dictionary{key}.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get %dictionary{key}.3)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{key}.4)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_DictionaryDefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(let %dictionary{key} value)", ref index).Value);
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get %dictionary{key})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);

			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(let %dictionary{} default)", ref index).Value);
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get %dictionary{key})", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_FieldDefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(let var f1|\"f 2\"|f3)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var.0)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get var.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get var.3)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var.4)", ref index).Value);

			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(let var.0 default)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get var.0)", ref index).Value);
			index = 0;
			Assert.AreEqual("f1", Evaluation.Parse("(get var.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("f 2", Evaluation.Parse("(get var.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("f3", Evaluation.Parse("(get var.3)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get var.4)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_DictionaryDefaultValue_FieldDefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("default|value", Evaluation.Parse("(let %dictionary{} default|value)", ref index).Value);
			index = 0;
			Assert.AreEqual("default|value", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{}.0)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get %dictionary{}.1)", ref index).Value);
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get %dictionary{}.2)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{}.3)", ref index).Value);

			index = 0;
			Assert.AreEqual("hello", Evaluation.Parse("(let %dictionary{}.0 hello)", ref index).Value);
			index = 0;
			Assert.AreEqual("world", Evaluation.Parse("(let %dictionary{}.1 world)", ref index).Value);

			index = 0;
			Assert.AreEqual("world|value", Evaluation.Parse("(get %dictionary{})", ref index).Value);
			index = 0;
			Assert.AreEqual("hello", Evaluation.Parse("(get %dictionary{}.5)", ref index).Value);
		}

		[TestMethod]
		public void GetFunction_DefaultValue()
		{
			int index;
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get x default)", ref index).Value);

			index = 0;
			Assert.AreEqual("f1|f 2|f3", Evaluation.Parse("(let var f1|\"f 2\"|f3)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get var.5)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get var.5 default)", ref index).Value);

			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary)", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get %dictionary default)", ref index).Value);

			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(let %dictionary{key} value)", ref index).Value);
			index = 0;
			Assert.AreEqual("", Evaluation.Parse("(get %dictionary{absent})", ref index).Value);
			index = 0;
			Assert.AreEqual("default", Evaluation.Parse("(get %dictionary{absent} default)", ref index).Value);
		}
		
		[TestMethod]
		public void GetFunction_Dictionary_EvaluableKey()
		{
			int index;
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(let %dictionary{key} value)", ref index).Value);
			index = 0;
			Assert.AreEqual("key", Evaluation.Parse("(let keyName key)", ref index).Value);
			
			index = 0;
			Assert.AreEqual("value", Evaluation.Parse("(get (concat %dictionary{ (get keyName) } ))", ref index).Value); // Full syntax
		}
	}
}