﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should;
using UpLang.Execution;

namespace Test.Execution
{
	[TestClass]
	public class ExecutorStatementTests : StatementTestsBase
	{
		[TestMethod]
		public void Executor_EmptyCode()
		{
			var code = String.Empty;
			Executor.Execute(code);
		}

		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void Executor_NullCode()
		{
			Executor.Execute(null);
		}
	}
}