﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;
using UpLang.Execution.Scopes;

namespace Test.Execution
{
	[TestClass]
	public class ScopeTests
	{
		[TestMethod]
		public void Scope_NameParsing_SetField()
		{
			Assert.AreEqual("f1|hello|f3|f4", Scope.SetField("f1|f2|f3|f4", 2, "hello"));
			Assert.AreEqual("hello|f2|f3|f4", Scope.SetField("f1|f2|f3|f4", 1, "hello"));
			Assert.AreEqual("f1|f2|f3|hello", Scope.SetField("f1|f2|f3|f4", 4, "hello"));
			Assert.AreEqual("f1|f2|f3|f4|hello", Scope.SetField("f1|f2|f3|f4", 5, "hello"));
			Assert.AreEqual("f1|f2|f3|f4||||||hello", Scope.SetField("f1|f2|f3|f4", 10, "hello"));

			Assert.AreEqual("f1||f3|f4", Scope.SetField("f1|f2|f3|f4", 2, ""));
			Assert.AreEqual("|f2|f3|f4", Scope.SetField("f1|f2|f3|f4", 1, ""));
			Assert.AreEqual("f1|f2|f3|", Scope.SetField("f1|f2|f3|f4", 4, ""));
			Assert.AreEqual("f1|f2|f3|f4|", Scope.SetField("f1|f2|f3|f4", 5, ""));
			Assert.AreEqual("f1|f2|f3|f4||||||", Scope.SetField("f1|f2|f3|f4", 10, ""));

			Assert.AreEqual("f1|f3|f4", Scope.SetField("f1|f2|f3|f4", 2, null));
			Assert.AreEqual("f2|f3|f4", Scope.SetField("f1|f2|f3|f4", 1, null));
			Assert.AreEqual("f1|f2|f3", Scope.SetField("f1|f2|f3|f4", 4, null));
			Assert.AreEqual("f1|f2|f3|f4", Scope.SetField("f1|f2|f3|f4", 5, null));
			Assert.AreEqual("f1|f2|f3|f4", Scope.SetField("f1|f2|f3|f4", 10, null));
		}

		[TestMethod]
		public void Scope_NameParsing_Correct()
		{

			Assert.IsTrue(Scope.ParseName("variable", out var dictionary, out var varkey, out var field));
			Assert.AreEqual(null, dictionary);
			Assert.AreEqual("variable", varkey);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual(null, varkey);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{}", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual(Consts.DEFAULT_KEY, varkey);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{key}", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual("key", varkey);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("variable.0", out dictionary, out varkey, out field));
			Assert.AreEqual(null, dictionary);
			Assert.AreEqual("variable", varkey);
			Assert.AreEqual(Consts.DEFAULT_FIELD, field);

			Assert.IsTrue(Scope.ParseName("variable.5", out dictionary, out varkey, out field));
			Assert.AreEqual(null, dictionary);
			Assert.AreEqual("variable", varkey);
			Assert.AreEqual(5, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{}.0", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual(Consts.DEFAULT_KEY, varkey);
			Assert.AreEqual(Consts.DEFAULT_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{}.5", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual(Consts.DEFAULT_KEY, varkey);
			Assert.AreEqual(5, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{key}.0", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual("key", varkey);
			Assert.AreEqual(Consts.DEFAULT_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{key}.5", out dictionary, out varkey, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual("key", varkey);
			Assert.AreEqual(5, field);
		}

		[TestMethod]
		public void Scope_NameParsing_CorrectEscaped()
		{

			Assert.IsTrue(Scope.ParseName("\\%v\\.ar\"ia\\.ble\"", out var dictionary, out var variable, out var field));
			Assert.AreEqual(null, dictionary);
			Assert.AreEqual("%v.aria.ble", variable);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%\"dic\\\"\"tionary\\{\\}", out dictionary, out variable, out field));
			Assert.AreEqual("dic\"tionary{}", dictionary);
			Assert.AreEqual(null, variable);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary{\"\"}", out dictionary, out variable, out field));
			Assert.AreEqual("dictionary", dictionary);
			Assert.AreEqual(Consts.DEFAULT_KEY, variable);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%dictionary\\{{\\}ke\"y\"}", out dictionary, out variable, out field));
			Assert.AreEqual("dictionary{", dictionary);
			Assert.AreEqual("}key", variable);
			Assert.AreEqual(Consts.NO_FIELD, field);

			Assert.IsTrue(Scope.ParseName("var\\.iable\".\".\"0\"", out dictionary, out variable, out field));
			Assert.AreEqual(null, dictionary);
			Assert.AreEqual("var.iable.", variable);
			Assert.AreEqual(Consts.DEFAULT_FIELD, field);

			Assert.IsTrue(Scope.ParseName("%\"hello world\"\\{\\}\\.{\\{странный\\ ключ\\}\\.}.\"4\"\"2\"", out dictionary, out variable, out field));
			Assert.AreEqual("hello world{}.", dictionary);
			Assert.AreEqual("{странный ключ}.", variable);
			Assert.AreEqual(42, field);
		}

		[TestMethod]
		public void Scope_NameParsing_Incorrect()
		{
			string dictionary;
			string variable;
			int field;

			Assert.IsFalse(Scope.ParseName("", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName(".", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName(".variable", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("{", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("{}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("{key}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("{key}.5", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable.", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable{", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable{}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable{}.5", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable{key}.5", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%dictionary{", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%dictionary}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%{", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%{}", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%dictionary.", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("%dictionary{key}.", out dictionary, out variable, out field));
			Assert.IsFalse(Scope.ParseName("variable.-5", out dictionary, out variable, out field));
		}

		[TestMethod]
		public void Scope_FlatScope()
		{
			var scope = new Scope(null);
			Assert.IsFalse(scope.Owns("test"));
			scope.Define("test", "value");
			Assert.AreEqual("value", scope.Resolve("test"));
			Assert.IsFalse(scope.Owns("TEST"));
			scope.Define("test", "new value");
			Assert.AreEqual("new value", scope.Resolve("test"));
			scope.Define("test", "");
			Assert.AreEqual("", scope.Resolve("test"));
			scope.Define("test", null);
			Assert.IsFalse(scope.Owns("test"));

			Assert.AreEqual(System.String.Empty, scope.Resolve("not exists"));
		}

		[TestMethod]
		public void Scope_InheritedScope()
		{
			var parent = new Scope(null);
			var child = new Scope(parent);
			Assert.IsFalse(child.Owns("test"));
			Assert.IsFalse(parent.Owns("test"));

			child.Define("test", "child");
			Assert.IsFalse(parent.Owns("test"));
			Assert.IsTrue(child.Owns("test"));
			Assert.IsTrue(child.Has("test"));
			Assert.AreEqual("child", child.Resolve("test"));

			parent.Define("test", "parent");
			Assert.IsTrue(parent.Owns("test"));
			Assert.IsTrue(parent.Has("test"));
			Assert.AreEqual("parent", parent.Resolve("test"));
			Assert.AreEqual("child", child.Resolve("test"));

			child.Define("test", null);
			Assert.IsFalse(child.Owns("test"));
			Assert.IsTrue(child.Has("test"));
			Assert.IsTrue(parent.Owns("test"));
			Assert.IsTrue(parent.Has("test"));
			Assert.AreEqual("parent", parent.Resolve("test"));
			Assert.AreEqual("parent", child.Resolve("test"));

			parent.Define("test", null);
			Assert.IsFalse(parent.Owns("test"));
			Assert.IsFalse(child.Owns("test"));
			Assert.IsFalse(parent.Has("test"));
			Assert.IsFalse(child.Has("test"));
		}

		[TestMethod]
		public void Scope_NestedScopes()
		{
			var root = new Scope(null);
			var parent = new Scope(root);
			var boy = new Scope(parent);
			var girl = new Scope(parent);
			var child = new Scope(girl);

			root.Define("root", "root value");
			parent.Define("parent", "parent value");
			Assert.AreEqual("root value", parent.Resolve("root"));
			Assert.AreEqual("root value", boy.Resolve("root"));
			Assert.AreEqual("root value", girl.Resolve("root"));
			Assert.AreEqual("root value", child.Resolve("root"));
			Assert.AreEqual("parent value", boy.Resolve("parent"));
			Assert.AreEqual("parent value", girl.Resolve("parent"));
			Assert.AreEqual("parent value", child.Resolve("parent"));

			boy.Define("boy", "boy's secret");
			Assert.IsFalse(root.Has("boy"));
			Assert.IsFalse(parent.Has("boy"));
			Assert.IsFalse(girl.Has("boy"));
			Assert.IsFalse(child.Has("boy"));

			girl.Define("girl", "milk");
			Assert.AreEqual("milk", girl.Resolve("girl"));
			Assert.AreEqual("milk", child.Resolve("girl"));
			Assert.IsFalse(root.Has("girl"));
			Assert.IsFalse(parent.Has("girl"));
			Assert.IsFalse(boy.Has("girl"));
		}
	}
}