﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should;
using UpLang.Parsing.Texts;

namespace Test.Parsing
{
	[TestClass]
	public class AsListTests
	{
		[TestMethod]
		public void EmptyText_ToList()
		{
			var empty = new EmptyText();
			empty.AsList().ShouldBeEmpty();
		}
		
		[TestMethod]
		public void RawText_ToList()
		{
			var raw1 = new RawText("hello");
			var list1 = raw1.AsList().ToArray();
			list1.ShouldBeSingle();
			list1[0].ToString().ShouldEqual("hello");

			var raw2 = new RawText("hello my world");
			var list2 = raw2.AsList().ToArray();
			list2.Length.ShouldEqual(3);
			list2[0].ToString().ShouldEqual("hello");
			list2[1].ToString().ShouldEqual("my");
			list2[2].ToString().ShouldEqual("world");

			var raw3 = new RawText("hello\t my   \tworld");
			var list3 = raw3.AsList().ToArray();
			list3.Length.ShouldEqual(3);
			list3[0].ToString().ShouldEqual("hello");
			list3[1].ToString().ShouldEqual("my");
			list3[2].ToString().ShouldEqual("world");
		}

		[TestMethod]
		public void EvaluableText_ToList()
		{
			var eval1 = new EvaluableText("(+ 2 3)");
			var list1 = eval1.AsList().ToArray();
			list1.ShouldBeSingle();
			list1[0].ToString().ShouldEqual("5");
			
			var eval2 = new EvaluableText("(list  hello\tmy \t  world)");
			var list2 = eval2.AsList().ToArray();
			list2.Length.ShouldEqual(3);
			list2[0].ToString().ShouldEqual("hello");
			list2[1].ToString().ShouldEqual("my");
			list2[2].ToString().ShouldEqual("world");
		}
	}
}
