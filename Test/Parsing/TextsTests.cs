﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Texts;

namespace Test.Parsing
{
	// Use snippets "testc" and "testm" to create unit tests quickly.

	[TestClass]
	public class TextsTests
	{
		[TestMethod]
		public void Text_RawText()
		{
			var raw = new RawText("hello");
			Assert.AreEqual("hello", raw);
		}

		[TestMethod]
		public void Text_EvalText()
		{
			Assert.AreEqual("11", new EvaluableText("(+ 5 6)"));
			Assert.AreEqual("11", new EvaluableText("(+ 5 (- 3 1) 4)"));
			Assert.AreEqual("sometext", new EvaluableText("(text sometext)"));
			Assert.AreEqual("some text", new EvaluableText("(text \"some text\")"));
		}

		[TestMethod]
		public void Text_ChainOfRawText()
		{
			var chain = new TextChain(
				new RawText("hello"),
				(RawText) "my",
				(RawText) "world"
			);
			Assert.AreEqual("hellomyworld", chain);
		}
	}
}