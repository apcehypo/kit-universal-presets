﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Extensions;
using UpLang.Parsing;

namespace Test.Parsing
{
	[TestClass]
	public class EscapedStringTests
	{
		[TestMethod]
		public void EscapedString()
		{
			EscapedString test;

			test = new EscapedString("hello world");
			Assert.AreEqual("hello world", test.Value);
			Assert.AreEqual("00000000000", test.Escaped.AsBinaryString());

			test = new EscapedString("hel\"lo wo\"rld");
			Assert.AreEqual("hello world", test.Value);
			Assert.AreEqual("00011111000", test.Escaped.AsBinaryString());

			test = new EscapedString("\\\"hello\\\" world");
			Assert.AreEqual("\"hello\" world", test.Value);
			Assert.AreEqual("1000001000000", test.Escaped.AsBinaryString());

			test = new EscapedString("\\\"he\\\"llo\\\" world");
			Assert.AreEqual("\"he\"llo\" world", test.Value);
			Assert.AreEqual("10010001000000", test.Escaped.AsBinaryString());
		}

		[TestMethod]
		public void EscapedString_Split()
		{
			EscapedString test;
			string[] parts;

			test = new EscapedString("hello   my world");
			parts = test.Split(' ', false);
			Assert.AreEqual(3, parts.Length);
			Assert.AreEqual("hello", parts[0]);
			Assert.AreEqual("my", parts[1]);
			Assert.AreEqual("world", parts[2]);

			parts = test.Split(' ', true);
			Assert.AreEqual(5, parts.Length);
			Assert.AreEqual("hello", parts[0]);
			Assert.AreEqual("", parts[1]);
			Assert.AreEqual("", parts[2]);
			Assert.AreEqual("my", parts[3]);
			Assert.AreEqual("world", parts[4]);

			test = new EscapedString("hello \"my wo\"rld");
			parts = test.Split(' ', false);
			Assert.AreEqual(2, parts.Length);
			Assert.AreEqual("hello", parts[0]);
			Assert.AreEqual("my world", parts[1]);

			test = new EscapedString("hello \\\"my wo\\\"rld");
			parts = test.Split(' ', false);
			Assert.AreEqual(3, parts.Length);
			Assert.AreEqual("hello", parts[0]);
			Assert.AreEqual("\"my", parts[1]);
			Assert.AreEqual("wo\"rld", parts[2]);
		}
	}
}