﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Should
{
	public static class ShouldExtensions
	{
		public static void ShouldContain<T>(this IEnumerable<T> collection, Predicate<T> expected, string message = "Collection does not contain expected element")
		{
			if (!collection.Any(item => expected(item)))
			{
				Assert.Fail(message);
			}
		}
		
		public static void ShouldNotContain<T>(this IEnumerable<T> collection, Predicate<T> expected, string message = "Collection contains unexpected element")
		{
			if (collection.Any(item => expected(item)))
			{
				Assert.Fail(message);
			}
		}
		
		public static void ShouldContain<T, TU>(this IEnumerable<T> collection, IEnumerable<TU> elements,
			Func<T, TU, bool> expected, string message = "Collection contains unexpected elements")
		{
			if (elements.Any(element => !collection.Any(x => expected(x, element))))
			{
				Assert.Fail(message);
			}
		}
		
		public static void ShouldBeSingle<T>(this IEnumerable<T> collection, Predicate<T> expected = null,
			string message = "Collection contains more or less than one element")
		{
			if (collection.Count(x => expected == null || expected(x)) != 1)
			{
				Assert.Fail(message);
			}
		}
		
		public static void ShouldBeNullOrEmpty(this string value, string message = "String is not empty")
		{
			string.IsNullOrEmpty(value).ShouldBeTrue(message);
		}

		public static void ShouldNotBeNullOrEmpty(this string value, string message = "String is null or empty")
		{
			string.IsNullOrEmpty(value).ShouldBeFalse(message);
		}

		public static void ShouldStructuralEqual<T>(this T expected, T result)
			where T : IStructuralEquatable
		{
			var message = $"Actual: {SerializeObject(result)} Expected: {SerializeObject(expected)}";

			expected.Equals(result, StructuralComparisons.StructuralEqualityComparer).ShouldBeTrue(message);
		}

		private static string SerializeObject(object value)
		{
			return JsonConvert.SerializeObject(value, Formatting.Indented);
		}
	}
}
